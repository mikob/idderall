#!/bin/bash

if [ -d ~/workspace/ShockCollar ]; then
    echo "Looking at ~/workspace/ShockCollar"
    
    if [ -d ~/workspace/ShockCollarDeploy ]; then
        echo "Deploying to ~/workspace/ShockCollarDeploy"

        # Plenty o dir recursion
        for file in ~/workspace/ShockCollarDeploy/*/*/*/*; do
            echo "$file"
            #newVersion=`echo $file | sed s/ShockCollarDeploy/ShockCollar/`
            #echo "$newVersion"
        done
    fi
fi
