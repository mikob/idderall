#!/bin/bash

rm -r ../data/dojo
rm -r ../lib/dojo_build/release
rm -r ../lib/dojo_build/custom/panels
rm -r ../lib/dojo_build/custom/util
rm -r ../lib/dojo_build/custom/widgets
cp -r ../data/panels ../lib/dojo_build/custom/panels
cp -r ../data/widgets ../lib/dojo_build/custom/widgets
cp -r ../data/util ../lib/dojo_build/custom/util

cd ../lib/dojo_build/util/buildscripts
./build.sh profileFile=/home/miko/workspace/ShockCollar/lib/dojo_build/shockcollar.profile.js action=release layerOptimize=shrinksafe

cd ~/workspace/ShockCollar
mv lib/dojo_build/release/dojo/dojo data/dojo


