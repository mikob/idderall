This is the idderall Firefox add-on.

* bin/
    Tools for development (not included in XPI build)
* lib/
    Contains the source files that interact directly with the add-on SDK
* data/
    Contains front-end add-on materials
    * data/frames
        Contains source for the block page, and the surrender pages
    * data/media 
        All static media including css
    * data/menu
        The menu widget pop-up
    * data/panels
        The dojo widgets that correspond to setup and settings panels
        * data/panels/templates
            The dojo widget templates
    * data/setup
        Setup and settings stuff
    * data/util
        A util dojo "class"
    * data/widgets
        Contains dojo widget overrides
* test/
    The add-on SDK tests
