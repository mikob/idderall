/* 
 * This content script is attached to the Setup html page and is used for 
 * communication between the add-on and the html js
 */
document.addEventListener('ShockCollar.SavePanelData', function(evt)
{
    /* Summary:
     *  Get the payload from the panels window.document js object...
     *
     *  eg.
     *      ShockCollar.CompiledBlockList or ShockCollar.CompiledBlockSchedule
     *
     *  Then emit the generic save event sending the proper payload of what
     *  needs to be saved.
     */
    var source = evt.target.getAttribute('source');
    console.log('setup.js SavePanelData source: ' + source);

    // if the user saves the schedule again, then re-initiate the blocks
    if (source == 'Deterrent' && unsafeWindow.ShockCollar.BlockReleased)
    {
        unsafeWindow.ShockCollar.BlockReleased = false;
        document.getElementById('block_released_notice').setAttribute('style', 
            'display: hidden;');
    }

	var payload = content.wrappedJSObject.ShockCollar['Compiled' + source];
    console.log('setup.js SavePanelData payload: ' + payload);
	self.port.emit('SavePanelData', [source, payload]); 
}, false, true);

document.addEventListener('ShockCollar.RetrievePanelData', function(evt)
{
    var source = evt.target.getAttribute('source');
    console.log('setup.js RetrievePanelData source: ' + source);
    self.port.emit('RetrievePanelData', source);
}, false, true);

document.addEventListener('ShockCollar.Close', function(evt)
{
    self.port.emit('Close');
}, false, true);

self.port.on('BlockReleased', function()
{
    $(document).ready(function() {
        if (typeof unsafeWindow.ShockCollar == 'undefined')
        {
            unsafeWindow.ShockCollar = {};
        }

        unsafeWindow.ShockCollar.BlockReleased = true;

        document.getElementById('block_released_notice').setAttribute('style', 
            'display: block;');
    });
});

self.port.on('LoadStep', function(stepName)
{
    /* Summary:
     *  Step name should be equivalent to source name, eg.
     *  BlockLists, BlockSchedule
     */
    if (typeof unsafeWindow.ShockCollar == 'undefined')
    {
        unsafeWindow.ShockCollar = {};
    }

    unsafeWindow.ShockCollar.CurrentStep = stepName;
});

self.port.on('LoadPanelData', function(payload)
{
    var source = payload[0];
    var loadedData = payload[1];
    console.log('setup.js LoadPanelData source: ' + source + ', loadedData: ' + loadedData);

    /* TODO: Is there a way to do this without using unsafe window?? */
    if (typeof unsafeWindow.ShockCollar == 'undefined')
    {
        unsafeWindow.ShockCollar = {};
    }

	unsafeWindow.ShockCollar[source] = loadedData;

    var element = document.createElement('ShockCollarDataElement');
    element.setAttribute('source', source);
    document.body.appendChild(element);
    var evt = document.createEvent('Events');
    evt.initEvent('ShockCollar.LoadPanelData', true, false);

    element.dispatchEvent(evt);

    console.log("event dispatched loadblockschedule");

});

document.addEventListener('ShockCollar.FinishedSetup', function(evt)
{
    console.log('setup.js FinishedSetup');
    self.port.emit('FinishedSetup');
}, false, true);

