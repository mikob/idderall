/*
 * Used in settings and setup
 */
dojo.addOnLoad(function() {
dojo.require("dojo.NodeList-traverse");
dojo.require("shockcollar.panels.Welcome");
dojo.require("shockcollar.panels.TermsAndConditions");
dojo.require("shockcollar.panels.Deterrent");
dojo.require("shockcollar.panels.BlockLists");
dojo.require("shockcollar.panels.BlockSchedule");
dojo.require("shockcollar.panels.Initiation");

dojo.ready(function() {
    // First turn off the loading screen
    dojo.style(dojo.byId('loading'), 'opacity', 1);
    var loadFade = dojo.fadeOut({
        node: dojo.byId('loading'),
        onEnd: function()
            {
                dojo.style(dojo.byId('loading'), 'display', 'none');
            }
        });
    loadFade.play();

    var that = this;
    this.navButtons = dojo.query("ol.steps li");

    // Load a step if setup was previously opened
    if (window.ShockCollar && window.ShockCollar.CurrentStep)
    {
        this.currentStep = stepNameToNum(window.ShockCollar.CurrentStep);
    }
    else
    {
        this.currentStep = 1;
    }

    this.STEPS = [	shockcollar.panels.Welcome, shockcollar.panels.TermsAndConditions,
            shockcollar.panels.BlockLists, shockcollar.panels.BlockSchedule, 
            shockcollar.panels.Deterrent, shockcollar.panels.Initiation ];
    this.MAX_STEPS = this.STEPS.length;
    
    this._nextBtn = dojo.byId("nextBtn");
    this._prevBtn = dojo.byId("prevBtn");

    // The exit control
    function promptForExit(/*bool*/ promptUser)
    {
        if (promptUser)
        {
            window.onbeforeunload = dojo.hitch(that, function(evt) {
                // silent validation and save
                if (this.curStepContent.validate())
                {
                    this.curStepContent.save();
                }
                alert("Some settings wont be saved and Idderall wont be initiated"
                + " until you click \"finish\" on the initiation step.");
                return false;
            });
        }
        else
        {
            window.onbeforeunload = null;
        }
    }

    function stepNameToNum(stepName)
    {
        var stepNum;
        switch (stepName) {
            case 'TermsAndConditions':
                stepNum = 2;
                break;
            case 'BlockLists':
                stepNum = 3;
                break;
            case 'BlockSchedule':
                stepNum = 4;
                break;
            case 'Deterrent':
                stepNum = 5;
                break;
            case 'Initiation':
                stepNum = 6;
                break;
            default:
                stepNum = 1;
                break;
        }
        return stepNum;
    };

    dojo.forEach(this.navButtons, function(navButton) {
        dojo.connect(navButton, 'onclick', navClicked);
    });

    dojo.connect(this._nextBtn, 'onclick', nextClicked);
    dojo.connect(this._prevBtn, 'onclick', prevClicked);

    initialize();

    function initialize()
    {
        selectStep(that.currentStep, true);
    }

    function navClicked(e /*event*/)
    {
        var selectedNode = e.target.parentNode;
    }

    function nextClicked()
    {
        nextStep();
    }

    function prevClicked()
    {
        prevStep();
    }

    function nextStep()
    {
        if (that.currentStep < that.MAX_STEPS)
        {
            // First check if the step is valid if the user
            // is proceeding forward!
            var valid = that.curStepContent.validate();
            if (valid !== true)
            {
                alert(that.curStepContent.invalidMsg);
            }
            else // we are valid!
            {
                if (that.curStepContent.beforeSave) {
                    var deferred = that.curStepContent.beforeSave();
                    deferred.then(function() {
                        selectStep(that.currentStep + 1, true);
                        that.currentStep++;
                    });
                }
                else 
                {
                    selectStep(that.currentStep + 1, true);
                    that.currentStep++;
                }
            }
        }
        else
        {
            finishedSetup();
        }
    }

    function finishedSetup()
    {
        // We're finished, send an event to let the add-on know
        var element = dojo.create('ShockCollarDataElement', {payload: ''}, dojo.body());
        var evt = document.createEvent('Events');
        evt.initEvent('ShockCollar.FinishedSetup', true, false);
        element.dispatchEvent(evt);

        console.log('Finished setup');
    }
    
    function prevStep()
    {
        if (that.currentStep > 1)
        {
            selectStep(that.currentStep - 1, false);
            that.currentStep--;
        }
    }

    function selectStep(stepNum, /*bool*/ next)
    {
        // Deal with the previous content, including save
        var saveDeferred, animDeferred;
        try 
        {
            if (typeof that.curStepContent != 'undefined') 
            {
                saveDeferred = that.curStepContent.save();

                // Next is true if this is a forward step. So we can
                // set the "right" [bool] param to true for anim(). The
                // second param is true because we are always moving
                // the panel out of the screen after a save 
                animDeferred = that.curStepContent.anim(!next, true);
            }
        }
        catch (err)
        {
            console.log('Error saving/animating panel: ' + err);
        }

        // In case there wasn't previous content
        if (typeof saveDeferred == 'undefined')
        {
            saveDeferred = new dojo.Deferred();
            saveDeferred.callback();
        }

        if (typeof animDeferred == 'undefined')
        {
            animDeferred = new dojo.Deferred();
            animDeferred.callback();
        }

        // Things that should happen only after the
        // save and animation exit sequence
        var savedAndAnimationDoneDeferred = new dojo.DeferredList([ saveDeferred, animDeferred]);
        savedAndAnimationDoneDeferred.then(dojo.hitch(that, function()
        {
            // Take care of buttons
            if (stepNum == that.MAX_STEPS)
            {
                promptForExit(false);
                // Last step
                that._prevBtn.disabled = false;
                that._nextBtn.textContent = "Finish";
            }
            else if (stepNum == 1)
            {
                promptForExit(false);
                that._prevBtn.disabled = true;
                that._nextBtn.textContent = "Next";
            }
            else
            {	
                promptForExit(true);
                that._prevBtn.disabled = false;
                that._nextBtn.textContent = "Next";
            }
        
            if (that.curStepContent)
            {
                that.curStepContent.destroyRecursive();

                // destroy EVERYTHING WORKAROUND!
                dijit.registry.forEach(function(w) {
                    w.destroyRecursive();
                });
                // Don't empty until we properly save and destroy
                dojo.empty(dojo.byId('content'));
            }

            that.curStepContent = that.STEPS[stepNum - 1]();

            // Load returns a deferred, calls it back when finished loading
            var loaded = that.curStepContent.load();
            loaded.then(function()
            {
                that.curStepContent.placeAt(dojo.byId('content'), 'last');
                that.curStepContent.startup();
                
                // Animate this baby in
                // the second param is false because we are animating in
                that.curStepContent.anim(next, false);

                var selectedNode = that.navButtons[stepNum - 1]; 
                var precedingSelectedDivNode = dojo.query(selectedNode).prev()[0]; 
                
                // Reset selected list item selected classes on all navButtons
                that.navButtons.forEach(function(navButton) {
                    dojo.removeClass(navButton, 'selected');
                });
                
                // Reset arrow css classes	
                dojo.query("div.arrowPrevious").forEach(function(arrow) {
                    dojo.removeClass(arrow, 'arrowPrevious');
                });
        
                dojo.toggleClass(selectedNode, 'selected');
                dojo.toggleClass(precedingSelectedDivNode, 'arrowPrevious');
            });
        }));
    }

});
});
