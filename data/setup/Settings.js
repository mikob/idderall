dojo.addOnLoad(function() {
dojo.require("shockcollar.panels.Deterrent");
dojo.require("shockcollar.panels.BlockLists");
dojo.require("shockcollar.panels.BlockSchedule");

dojo.ready(function() {
    // First turn off the loading screen
    dojo.style(dojo.byId('loading'), 'opacity', 1);
    var loadFade = dojo.fadeOut({
        node: dojo.byId('loading'),
        onEnd: function()
        {
            dojo.style(dojo.byId('loading'), 'display', 'none');
        }
    });
    loadFade.play();

    var that = this;
    var panel;
    var navButtons = dojo.query('.navButton');
    var currentPanel = 'BlockLists';
    this.properSave = false;

    // The exit control
    window.onbeforeunload = dojo.hitch(this, function(evt) {
        if (!this.properSave)
        {
            alert("Currently visible settings wont be saved "
            + " unless you click the \"Save & Close\" button!");
            return false;
        }
    });

    loadPanel(currentPanel);

    // connect events
    dojo.forEach(navButtons, function(navButton) {
        dojo.connect(navButton, 'onclick', that, navClicked);
    });

    var resetBtn = dojo.byId('resetBtn');
    dojo.query('.saveBtn').onclick(saveAndQuit);

    dojo.connect(resetBtn, 'onclick', resetClicked);

    function resetClicked()
    {
        loadPanel(currentPanel);
    }

    function saveAndQuit()
    {
        that.properSave = true;
        var valid = panel.validate();

        if (valid !== true)
        {
            alert(panel.invalidMsg);
        }
        else
        {
            var deferred;
            
            if (panel.beforeSave) {
                deferred = panel.beforeSave();
            }
            else
            {
                deferred = new dojo.Deferred();
                deferred.callback();
            }

            deferred.then(function() {
                var saved = panel.save();
                saved.then(function()
                {
                    // Send an event to the CS
                    var element = dojo.create('ShockCollarDataElement', null, dojo.body());
                    var evt = document.createEvent('Events');
                    evt.initEvent('ShockCollar.Close', true, false);
                    element.dispatchEvent(evt);
                });
            });
        }
    }

    function loadPanel(name)
    {
        // Get rid of the old panel
        currentPanel = name;
        if (panel)
        {
            try
            {
                panel.destroy();
            }
            catch (err)
            {
                console.log('Could not destroy settings panel: ' + err);
            }
        }
        dojo.empty(dojo.byId('content'));

        // Load the new panel
        panel = new shockcollar.panels[name]();
        var loaded = panel.load();
        loaded.then(function()
        {
            panel.placeAt(dojo.byId('content'), 'last');
            panel.startup();
        });
    }

    function navClicked(e /*event*/)
    {
        dojo.stopEvent(e);

        // get the panel name we need to load 
        var panelName = dojo.attr(e.target, 'value');

        if (panelName != currentPanel)
        {
            // First we send a signal to save the panel data?
            var valid = panel.validate();
            if (valid !== true)
            {
                alert(panel.invalidMsg);
                return false;
            }
            else // we are valid
            {
                var deferred;

                if (panel.beforeSave) {
                    deferred = panel.beforeSave();
                }
                else
                {
                    deferred = new dojo.Deferred();
                    deferred.callback();
                }

                deferred.then(function() {
                    var selectedParentNode = dojo.query('li a[value$=' + panelName + ']')
                        .parent()[0];
                    dojo.forEach(navButtons.parent(), function(navButton) {
                        dojo.toggleClass(navButton, 'selected', false);
                    });
                    dojo.toggleClass(selectedParentNode, 'selected', true);

                    var saved = panel.save();
                    saved.then(function()
                    {
                        loadPanel(panelName);
                    });
                });
            }
        }
    }
});
});
