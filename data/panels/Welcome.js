(function(){
dojo.provide('shockcollar.panels.Welcome');

dojo.require('shockcollar.panels.Panels');

dojo.declare('shockcollar.panels.Welcome', [shockcollar.panels.Panels], {
	templateString: dojo.cache('shockcollar.panels', 'templates/Welcome.html'),

	constructor: function() {

	},
	
	postCreate: function() {
		this.inherited(arguments);
	},

	startup: function() {
		this.inherited(arguments);
	}


});
})();
