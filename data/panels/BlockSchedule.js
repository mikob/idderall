(function(){
dojo.provide('shockcollar.panels.BlockSchedule');

dojo.require('dojo.data.ItemFileReadStore');
dojo.require('dojo.date.locale');
dojo.require('dijit.tree.ForestStoreModel');
dojo.require('dijit.form.TextBox');
dojo.require('dijit.Tooltip');
dojo.require('dijit.form.CheckBox');
dojo.require('dijit.form.NumberSpinner');
dojo.require('dijit.form.TimeTextBox');

dojo.require('shockcollar.panels.Panels');
dojo.require('shockcollar.widgets.Tree');

dojo.declare('shockcollar.panels.BlockSchedule', [shockcollar.panels.Panels], {
	templateString: dojo.cache('shockcollar.panels', 'templates/BlockSchedule.html'),

    PANEL_NAME: 'BlockSchedule',
	TREE_WIDTH: '280px',
	TREE_HEIGHT: '320px',
    TIME_PATTERN: 'H:mm',
    DEFAULT_START_TIME: '8:00',
    DEFAULT_END_TIME: '17:00',
    // The property in the store that contains the key (id)
    STORE_ID_PROP: 'name',
    DEFAULT_STORE_ID: 'Group 1',

	blockListTree: null,
	blockListTreeModel: null,
	blockListStore: null, // Where the user selects the group to change schedule of

    // An object that saves the schedule information, the group name (ID) is the key
    // for getting a group's saved values.
    // eg
    // { groupID : { props }, groupID : { props }, ...}
	savedGroupSchedule: null,
	currentBlockListGroupID: null,

	startup: function() {
		this.inherited(arguments);
		var that = this;

        // Load the store from the previous block list store
        var store;
        try
        {
            store = JSON.parse(window.ShockCollar.CompiledBlockLists);
        }
        catch (err)
        {
            console.log('Could not parse CompiledBlockLists: ' + err);
        }

        this.blockListStore = new dojo.data.ItemFileReadStore({
            data: store
        });

        this.blockListTreeModel = new dijit.tree.ForestStoreModel({
            store: this.blockListStore,
            childrenAttrs: ['children']
        });

		this.blockListTree = new shockcollar.widgets.Tree({
			model: this.blockListTreeModel,
			minSize: this.MIN_TREE_SIZE,
			style: 'width:' + this.TREE_WIDTH + ';height:' + this.TREE_HEIGHT + ';',
			persist: true 		// Cookie saving
		});

		this.blockListTree.placeAt(this._userBlockLists);

		this._connections.push(dojo.connect(this._cheatModeInput, 'onChange', function() {
			that._cheatModeMinsInput.set('disabled', !that._cheatModeInput.get('checked'));
		}));

		this._connections.push(dojo.connect(this._allDayInput, 'onChange', function() {
			that._timeStart.set('disabled', that._allDayInput.get('checked'));
			that._timeEnd.set('disabled', that._allDayInput.get('checked'));
		}));

		this._connections.push(dojo.connect(this.blockListTree, 'onClick', 
            dojo.hitch(this, this.treeSelectionChanged)));

		new dijit.Tooltip({
			connectId: ['cheatModeInput', 'cheatModeInputLabel'],
			position: 'before',
			label: 'Allows you to go on a blocked site unpenalized for<br>\
				a specified number of minutes.'
		});

		this.currentBlockListGroupID = this.blockListStore.getValue
            (this.blockListTree.getSelectedGroup(true, true), this.STORE_ID_PROP,
                this.DEFAULT_STORE_ID);

        // Call this in case a schedule was loaded in already
		var group = this.blockListTree.getSelectedGroup(true, true);

        this.loadGroupSchedule(group);
	},

	validate: function()
	{
		/* VIRTUAL
		 * Summary:
		 * 	* Check sanity of cheat minutes
		 * 	* Make sure we're under the 5mb storage quota
         * 	* Make sure start time is less then end time
		 */
        // Save the current group settings before we validate
        // (The final save before sending save trigger as well)
        this.saveCurrentGroupSchedule(this.currentBlockListGroupID);

        // Make sure we have valid times saved
        for (groupID in this.savedGroupSchedule)
        {
            var timeStart = dojo.date.locale.parse(this.savedGroupSchedule[groupID].activeTime.timeStart,
                    {
                        timePattern: this.TIME_PATTERN,
                        selector: 'time'
                    }
            );
            var timeEnd = dojo.date.locale.parse(this.savedGroupSchedule[groupID].activeTime.timeEnd,
                    {
                        timePattern: this.TIME_PATTERN,
                        selector: 'time'
                    }
            );
            if (timeStart > timeEnd)
            {
                this.invalidMsg = "C'mon now... Start time for " + groupID 
                        + " must be less than end time.";
                return false;
            } 
            else if (!(timeStart > timeEnd) && !(timeStart < timeEnd))
            {
                this.invalidMsg = "You can do this, right? Start time for " + groupID 
                        + " cannot be equal to end time.";
                return false;
            }

        }
		return true;
	},

	save: function()
	{
		/* VIRTUAL
		 * Summary:
		 * 	Save any settings pertinent to this 
		 * 	panel.
         * Note:
         *  Validation needs to be done before this is called.
         *  The currently selected group should be saved in validation
         *  as well.
		 */
        this.inherited(arguments);

		// Save is async
		var saveDeferred = new dojo.Deferred();
		var compiledSchedule = this.savedGroupSchedule;
        var that = this;

        // If we ever need to append extra items to the saved
        // blockList
        /*
        this.blockListStore.fetch({
            query: { 'type': 'group' },
            onItem: function(item) {
                var groupName = that.blockListStore.getValue(item, 'name'); 
                var schedule = that.getGroupSchedule(item);

                // Append a group name to the schedule item so we
                // can identify it later
                schedule.group = groupName;
                
                compiledSchedule.push(schedule);
            }
        });
        */

        window.ShockCollar['Compiled' + this.PANEL_NAME] = JSON.stringify(compiledSchedule);

        this.sendSaveTrigger(this.PANEL_NAME);

        // Done saving at this point
        saveDeferred.callback();

		return saveDeferred;
	},

    load: function()
    {
        /* Summary:
         *  Loaded is an overriden virtual function from Panels.js. We simply
         *  pass the name of this panel type to the base method and a series
         *  of steps take place to load necessities from the add-on storage.
         *  The base virtual method returns a deferred object. The load for
         *  this panel in particular has a global js object "window.ShockCollar
         *  .CompiledBlockSchedule" set after the loading is finished. This 
         *  object will contain the loaded data.
         * Details:
         *  We also need to make sure that we have block list information for the
         *  block schedule to work. So we will check if thats loaded here as well.
         */
        var that = this;
        var loaded = this.inherited(arguments, [[this.PANEL_NAME, 'BlockLists']]);

        // Any extra specific steps to take place after loading is done go in
        // this deferred.then function
        loaded.then(function() 
        {
            try
            {
                // Compiled is what the browser side code puts out for the add on code
                that.savedGroupSchedule = JSON.parse(window.ShockCollar[that.PANEL_NAME]);
                if (that.savedGroupSchedule == null)
                {
                    that.savedGroupSchedule = {};
                }
                window.ShockCollar.CompiledBlockLists = window.ShockCollar.BlockLists;
            }
            catch (err)
            {
                console.log('Could not load block schedule: ' + err)
            }
        });

        return loaded;
    },

	treeSelectionChanged: function()
	{
		this.updateGroupSchedule();
	},

	getGroupSchedule: function(/*dojo data item*/ group)
	{
		/* Summary:
		 * 	Get the group settings if they are stored,
		 * 	return defaults if not.
		 */

        // The id of the group is just the string name
        // eg. Group 1, Group 2... etc
        // Default argument is 'Group 1'
		var groupID = this.blockListStore.getValue(group, this.STORE_ID_PROP,
            this.DEFAULT_STORE_ID);

        // Find the saved schedule that matches the one we're getting
        var savedSchedule = this.savedGroupSchedule[groupID];

		if (typeof savedSchedule == 'undefined')
		{
			// Load default settings
			savedSchedule = {
				activeDays: 
				{
					sunday: false,
					monday: false,
					tuesday: false,
					wednesday: false,
					thursday: false,
					friday: false,
					saturday: false
				},
				activeTime: 
				{
					allDay: true,
					timeStart: this.DEFAULT_START_TIME,
					timeEnd: this.DEFAULT_END_TIME
				},
				cheatMode: true,
				cheatMins: 5
			};
		}

		return savedSchedule;
	},

	updateGroupSchedule: function()
	{
		var newGroup = this.blockListTree.getSelectedGroup(true, true);
		var newGroupID = this.blockListStore.getValue(newGroup, 
                this.STORE_ID_PROP, this.DEFAULT_STORE_ID); 

		if (newGroupID != this.currentBlockListGroupID) 
		{
			// Save the current settings before we change the current block
			// list group id
			this.saveCurrentGroupSchedule(this.currentBlockListGroupID);

			// Change the block list group physically
			this.loadGroupSchedule(newGroup);
		}
	},

	saveCurrentGroupSchedule: function(id)
	{
		/* Summary:
		 * 	Save all the value parameters in the group
		 * 	settings related controls in a 'dictionary'
		 * 	(really an associative array in js)
		 * 	where the the key is the group id.
		 */

		this.savedGroupSchedule[id] = {
			activeDays: 
			{
				sunday: this._sundayInput.get('checked'),
				monday: this._mondayInput.get('checked'),
				tuesday: this._tuesdayInput.get('checked'),
				wednesday: this._wednesdayInput.get('checked'),
				thursday: this._thursdayInput.get('checked'),
				friday: this._fridayInput.get('checked'),
				saturday: this._saturdayInput.get('checked')
			},
			activeTime: 
			{
				allDay: this._allDayInput.get('checked'),
                // Format the time for use
                timeStart: this._parseTimeFromTB(this._timeStart),
                timeEnd: this._parseTimeFromTB(this._timeEnd)
			},
			cheatMode: this._cheatModeInput.get('checked'),
			cheatMins: this._cheatModeMinsInput.get('value')
		};

	},

    _parseTimeFromTB: function(tb)
    {
        /* Summary:
         *  We have this in here in case the following situation
         *  occurs: the user clicks a tree item, 
         *  the current group is saved, but there is a problem parsing
         *  the time on the time text box for one reason or another.
         *  This helps ignore the parsing error, so it can be looked
         *  at during validation of the panel.
         */
        var parsedTime = "";

        try
        {
            parsedTime = dojo.date.locale.format(tb.get('value'),
            {
                timePattern: this.TIME_PATTERN,
                selector: 'time'
            });
        }
        catch (err) 
        {
            console.log('Could not parse time! ' + err); 
        };
        
        return parsedTime;
    },

	loadGroupSchedule: function(/*dojo data item*/ group)
	{
		/* Summary:
		 * 	Load the saved settings onto the sidebar 
		 * 	settings widget.
		 */
		var savedSettings = this.getGroupSchedule(group);
		var groupName = this.blockListStore.getValue(group, 'name');
		var groupID = this.blockListStore.getValue(group, this.STORE_ID_PROP,
            this.DEFAULT_STORE_ID); 
		var sunday, monday, tuesday, wednesday, thursday, friday, saturday,
			allDay, timeStart, timeEnd, cheatMode, cheatMins;

		// Title
		dojo.byId('groupScheduleLabel').innerHTML = groupName + ' Schedule';

		// Load the saved settings
		sunday = savedSettings.activeDays.sunday;
		monday = savedSettings.activeDays.monday;
		tuesday = savedSettings.activeDays.tuesday;
		wednesday = savedSettings.activeDays.wednesday;
		thursday = savedSettings.activeDays.thursday;
		friday = savedSettings.activeDays.friday;
		saturday = savedSettings.activeDays.saturday;
		allDay = savedSettings.activeTime.allDay;
		timeStart = savedSettings.activeTime.timeStart;
		timeEnd = savedSettings.activeTime.timeEnd;
		cheatMode = savedSettings.cheatMode;
		cheatMins = savedSettings.cheatMins;

        // Format the time before setting it (we can only set the widgets
        // using date objects)
        timeStart = dojo.date.locale.parse(timeStart,
            {
                timePattern: this.TIME_PATTERN,
                selector: 'time'
            });
        timeEnd = dojo.date.locale.parse(timeEnd,
            {
                timePattern: this.TIME_PATTERN,
                selector: 'time'
            });

		// Set control values to saved settings
		this._sundayInput.set('checked', sunday);
		this._mondayInput.set('checked', monday);
		this._tuesdayInput.set('checked', tuesday);
		this._wednesdayInput.set('checked', wednesday);
		this._thursdayInput.set('checked', thursday);
		this._fridayInput.set('checked', friday);
		this._saturdayInput.set('checked', saturday);
		this._allDayInput.set('checked', allDay);
		this._timeStart.set('value', timeStart);
		this._timeEnd.set('value', timeEnd);
		this._cheatModeInput.set('checked', cheatMode);
		this._cheatModeMinsInput.set('value', cheatMins);

        // Safe to change the block list id now
        this.currentBlockListGroupID = groupID;
	}


});
})();
