(function(){
dojo.provide('shockcollar.panels.Panels');

dojo.require('dijit._Widget');
dojo.require('dijit._Templated');
dojo.require('dojo.fx.easing');

dojo.require('shockcollar.util.Util');

dojo.declare('shockcollar.panels.Panels', [dijit._Widget, dijit._Templated], {

	widgetsInTemplate: true,
	_connections: null,

    ANIM_DUR: 600,
    ANIM_EASING: dojo.fx.easing.quadInOut,

    // Set this message to the reason why validate() returned false (failed)
    // it will be displayed to the user
    invalidMsg: null,

	util: null, 	// The singleton instance of util that all subclasses share

	constructor: function() 
	{
		//this.inherited(arguments);
		this._connections = [];
		this.util = new shockcollar.util.Util();
	},

	postMixInProperties: function() 
	{
		this.inherited(arguments);
	},

	postCreate: function() 
	{
		this.inherited(arguments);
	},

    startup: function()
    {
        /* Summary:
         *  Anything that needs data that is loaded in needs to be called in 
         *  this startup() function or later. For example the block lists
         *  page needs to load the blocklists before it can show the block tree
         *  so the block tree needs to be initialized in startup so it can have
         *  the loaded data.
         */
        this.inherited(arguments);
        this.hideError();
    },

    anim: function(/*bool*/ right, /*bool*/ out)
    {
        /* Summary:
         *  Animate out of the panel, callback the returned deferred
         *  when done.
         * right [bool]:
         *  Set to true if we need to animate to the right (start
         *  at center and move off the right edge of the screen, or 
         *  start off the right edge and move back in the middle), false
         *  to animate left.
         * out [bool]:
         *  Move out the screen if true; as opposed to coming into the
         *  field of view.
         */

        var deferred = new dojo.Deferred();
        var viewPort = dojo.window.getBox();
        var animNode = dojo.byId('boxWrapper');
        var origPos = dojo.position(animNode, true);
        var startAnimPos, endAnimPos;
        
        // Start by assuming out is false, (so we're moving the panel into the screen)
        endAnimPos = {
            x: origPos.x,
            y: origPos.y
        };

        if (right) 
        {
            startAnimPos = { 
                x: viewPort.w + origPos.x,
                y: origPos.y
            };
        } 
        else /*left*/
        {
            startAnimPos = { 
                x: -1 * viewPort.w - origPos.x,
                y: origPos.y
            };
        }

        // Swap if we're moving off the screen
        if (out)
        {
            var temp = startAnimPos;
            startAnimPos = endAnimPos;
            endAnimPos = temp;
        }

        // We can set the position to absolute after we have obtained the centered coordinates.
        // We can also set to visibility to visible now that we have the panel outside of the visible bounds
        dojo.style(animNode, 
            {
                position: 'absolute',
                left: startAnimPos.x + 'px',
                top: startAnimPos.y + 'px',
                visibility: 'visible'
            }); 

        dojo.fx.slideTo({ 
            easing: this.ANIM_EASING,
            node: animNode,
            left: endAnimPos.x,
            top: endAnimPos.y,
            duration: this.ANIM_DUR,
            onEnd: dojo.hitch(this, function()
                {
                    // Back to the original styling so the next panel is ready
                    // We need to end hidden if we're animating out so that when
                    // the next widget starts up it doesnt flash visible for a second
                    // before the animation has positioned it to the start
                    dojo.style(animNode, 
                        {
                            position: 'inherit',
                            visibility: out ? 'hidden' : 'visible'
                        }); 
                    deferred.callback();
                })
        }).play();

        return deferred;
    },
	
    showError: function(msg) {
        var notificationEle = dojo.query('#error_notification p')[0];
        notificationEle.innerHTML = msg;
        dojo.style(dojo.query('#error_notification')[0], 'display', 'block');
    },

    hideError: function() {
        var notificationEle = dojo.query('#error_notification p')[0];
        notificationEle.innerHTML = '';
        dojo.style(dojo.query('#error_notification')[0], 'display', 'none');
    },

	uninitialize: function() 
	{
		try
		{
			dojo.forEach(this._connections, function(connection) 
			{
				dojo.disconnect(connection);
			});
		}
		catch (err)
		{

		}
	},

	validate: function()
	{
		// Virtual
		return true;
	},

	save: function()
	{
		// Virtual	
        if (!window.ShockCollar)
        {
            window.ShockCollar = {};
        }
	},

    sendSaveTrigger: function(source)
    {
        /* source (string):
         *  The panel name that the save trigger is triggered for
         */
        var element = dojo.create('ShockCollarDataElement', 
            { 'source': source }, dojo.body());
        var evt = document.createEvent('Events');
        evt.initEvent('ShockCollar.SavePanelData', true, false);
		element.dispatchEvent(evt);
    },

    load: function(objsToLoad)
    {
        // Virtual
        /* Summary:
         *  Panel data that needs to be loaded uses this generic loader
         * objsToLoad [arr of strings]
         *  The arguments to send as 'source' to the add-on (what data
         *  to tell it to load)
         */
        var that = this;
        var loaded = new dojo.Deferred();        

        if (typeof objsToLoad == 'undefined')
        {
            loaded.callback();
        }
        else
        {
            var loadedObjs = [];

            document.addEventListener('ShockCollar.LoadPanelData',
                dojo.hitch(this, function(evt)
                {
                    // Only after all the sent arguments have been loaded
                    // in essence, we have a deferred list here
                    function arrays_equal(arr1, arr2)
                    {
                        if (arr1.length != arr2.length)
                            return false;

                        var numContains = 0;
                        for (var i = 0; i < arr1.length; i++)
                        {
                            for (var k = 0; k < arr2.length; k++)
                            {
                                if (arr1[i] == arr2[k])
                                {
                                    numContains++;
                                }
                            }
                        }
                        return numContains == arr1.length;
                    }

                    loadedObjs.push(evt.target.getAttribute('source'));

                    if (arrays_equal(loadedObjs, objsToLoad))
                    {
                        loaded.callback(); 
                    }
                }));

            for (var i = 0; i < objsToLoad.length; i++)
            {
                var source = objsToLoad[i];
                var element = dojo.create('ShockCollarDataElement', 
                    { source: source }, dojo.body());
                var evt = document.createEvent('Events');
                evt.initEvent('ShockCollar.RetrievePanelData', true, false);

                // It is safe to dispatch the retrieve event once we have the listener ready
                element.dispatchEvent(evt);
            }
        }

        return loaded;
    }

});
})();
