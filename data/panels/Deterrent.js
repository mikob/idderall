(function(){
dojo.provide('shockcollar.panels.Deterrent');

dojo.require('dojox.fx');
dojo.require('dojo.NodeList-traverse'); 
dojo.require('dijit.Dialog');
dojo.require('shockcollar.panels.Panels');
dojo.require('dijit.layout.ContentPane');

dojo.declare('shockcollar.panels.Deterrent', [shockcollar.panels.Panels], {
	templateString: dojo.cache('shockcollar.panels', 'templates/Deterrent.html'),

    PANEL_NAME: 'Deterrent',
    FADE_DUR: 400,
    FADE_OPAC: 0.43,
    EMAIL_HINT: "myfriend@example.com",
    // SHOCKCOLLAR_DOMAIN: 'http://idderall.com',
    SHOCKCOLLAR_DOMAIN: 'http://127.0.0.1:8000',

    MSG_PAYPAL_REG_ERROR: 'Error registering PayPal info with servers. Please contact '
        + 'support@idderall.com if problem persists.',
    MSG_FACEBOOK_REG_ERROR: 'Error registering with Facebook. Please contact '
        + 'support@idderall.com if problem persists.',
    MSG_FACEBOOK_INCORRECT: 'Error gathering permissions from Facebook. You must '
        + 'give idderall sufficient permissions.', 
    MSG_SERVER_ERROR: 'Error contacting server. Please check your connection'
        + ' and try again.', 
    MSG_EMAIL_ERROR: 'Error contacting e-mail server. Please check your connection'
        + ' and try again.',

    //The next three items are loaded in if they we're previously saved
    _selectedItem: null,
    _currencyAmount: null,
    _email: null,

    _groupedColumns: null,
    _beforeSaveDeferred: null,
    _emailDialog: null,

    _accessToken: null,
    _fbId: null,
    _instanceKey: null,

	startup: function() {
		this.inherited(arguments);
        var that = this;

        this._groupedColumns = dojo.query('.groupedColumn');
        var anchors = dojo.query('.option');

        // we might have already have a selected item
        // from the load
        if (this._selectedItem != null)
        {
            this._selectItem(
                dojo.query('[value=' + this._selectedItem + ']')[0]
            );

            this._tbCurrencyAmount.set('value', that._currencyAmount);
        }

        anchors.forEach(function(anchorNode, index, arr)
        {
            that._connections.push(dojo.connect(anchorNode, 'onclick', 
                    that, dojo.partial(that._selectItem, anchorNode)));
        });

        // The buttons on the blocking facebook overlay
        this._connections.push(dojo.connect(dojo.byId('btnRetryFB'), 'onclick', this, function()
            {
                that._submitFacebookInfo();
            }));

        this._connections.push(dojo.connect(dojo.byId('btnCancelFB'), 'onclick', this, function()
            {
                dojo.style(dojo.byId('screenBlocker'), 'display', 'none');
                that._selectItem(null);
            }));
        
		this._connections.push(dojo.connect(this._tbEmail, 'onFocus', this, this.hideEmailHint));
		this._connections.push(dojo.connect(this._tbEmail, 'onBlur', this, this.showEmailHint));

        this._emailDialog = new dijit.Dialog({
            title: 'E-mail Form',
            style: 'width: 500px;',
            onHide: dojo.hitch(this, function() {
                this._selectItem(null);
            }),
            href: this.SHOCKCOLLAR_DOMAIN + '/key/register',
            preload: true,
            onLoad: dojo.hitch(this, function() {
                this._connections.push(dojo.connect(dojo.byId('btnSubmitEmail'), 'onclick', this, this._submitEmailForm));
                this._connections.push(dojo.connect(dojo.byId('btnCancelEmail'), 'onclick', this, this._cancelEmailForm));
            })
        });

        if (this._email)
        {
            this._tbEmail.set('value', this._email);
        }
        else
        {
            this.showEmailHint();
        }

        // Initialize facebook stuff
        FB.init({appId: '135451879893783'});
	},

	showEmailHint: function()
	{
		var box = this._tbEmail;
		// Make sure the box isn't focused if we're going to 
		// show the hint...
		if (!box.get('focused'))
		{
			if (!dojo.trim(box.get('value')))
			{
				box.set('value', this.EMAIL_HINT);
				dojo.addClass(box.domNode, 'textBoxHint');
			}
		}
	},

	hideEmailHint: function()
	{
		var box = this._tbEmail;
		if (box.get('value') == this.EMAIL_HINT)
		{
			dojo.removeClass(box.domNode, 'textBoxHint');
			box.set('value', '');
		}
	},

    _selectItem: function(anchorNode)
    {
        var that = this;

        if (!anchorNode)
        {
            this._selectedItem = null;

            // beforeSave needs to start all over
            try
            {
                if (this._beforeSaveDeferred)
                {
                    this._beforeSaveDeferred.destroy();
                    this._beforeSaveDeferred = null;
                }
            }
            catch(err) { }


            this._groupedColumns.forEach(function(groupColumnNode, index, arr)
            {
                // The deselected nodes handled in here...
                dojox.fx.fadeTo({
                    node: groupColumnNode,
                    end: 1.0,
                    duration: that.FADE_DUR
                }).play();

                dojo.toggleClass(groupColumnNode, 'selected', false);
                dojo.toggleClass(groupColumnNode, 'deselected', false); 
            });
        }
        else
        {
            var selectedItem = dojo.attr(anchorNode, 'value');
            var clickedGroupColumnNode = dojo.NodeList(anchorNode).parent()[0];
            this._selectedItem = selectedItem;

            this._groupedColumns.forEach(function(groupColumnNode, index, arr)
            {
                if (groupColumnNode !== clickedGroupColumnNode) 
                {
                    // The deselected nodes handled in here...
                    dojox.fx.fadeTo({
                        node: groupColumnNode,
                        end: that.FADE_OPAC,
                        duration: that.FADE_DUR
                    }).play();

                    dojo.toggleClass(groupColumnNode, 'selected', false);
                    dojo.toggleClass(groupColumnNode, 'deselected', true); 
                }
            });

            // in case the clicked node was faded out...
            dojox.fx.fadeTo({
                node: clickedGroupColumnNode,
                end: 1.0,
                duration: that.FADE_DUR
            }).play();

            dojo.toggleClass(clickedGroupColumnNode, 'selected', true);
            dojo.toggleClass(clickedGroupColumnNode, 'deselected', false); 
        }

    },

    beforeSave: function() {
        /* Summary:
         *  Occurs before save, but after validation
         */
        this._beforeSaveDeferred = new dojo.Deferred();

        if (this._selectedItem == 'password')
        {
            this._emailDialog.show();

            // Autopopulate email field
            dojo.byId('id_recepient_email').value = this._tbEmail.get('value');
        } 
        else if (this._selectedItem == 'embarrassment')
        {
            this._submitFacebookInfo();
        }
        else if (this._selectedItem == 'currency')
        {
            this._submitPaypalInfo();
        }

        return this._beforeSaveDeferred;
    },

    _cancelEmailForm: function() {
        this._emailDialog.hide();
        this._selectItem(null);
    },

    uninitialize: function()
    {
        try
        {
            this._emailDialog.destroy();
        } catch(err) { }

        try
        {
            this._beforeSaveDeferred.destroy();
        } catch(err) { }
    },

    _submitPaypalInfo: function() 
    {
        /* Summary:
         *  Save PayPal data to server, so we can open an account for the user
         *  and remember how much the user was supposed to pay by looking
         *  up their instance id.
         */  
        var that = this;

        function serverError(msg)
        {
            that.showError(msg);
        }

        var amount = this._tbCurrencyAmount.get('value');

        var xhrArgs = {
            url: this.SHOCKCOLLAR_DOMAIN + '/paypal/register',
            content: {
                'amount': amount
            },
            load: function(response)
            {
                var responseObj = {};

                try
                {
                    responseObj = JSON.parse(response);
                }
                catch (err)
                {
                    responseObj.success = false;
                }

                if (responseObj.success === true)
                {
                    that._instanceKey = responseObj.data;
                    that._beforeSaveDeferred.callback();
                }
                else
                {
                    serverError(that.MSG_PAYPAL_REG_ERROR);
                }
            },
            error: function(response)
            {
                serverError(that.MSG_SERVER_ERROR);
            }
        };
        dojo.xhrPost(xhrArgs);
    },

    _submitEmailForm: function() {
        var that = this;
        var btnSubmitEmail = dojo.byId('btnSubmitEmail');
        btnSubmitEmail.disabled = true;
        btnSubmitEmail.innerHTML = "Sending...";

        function serverError(msg)
        {
            // Make sure to repop the previous values on reload
            var recepientName = dojo.byId('id_recepient_name').value;
            var senderName = dojo.byId('id_sender_name').value;
            var recepientEmail = dojo.byId('id_recepient_email').value;

            var refreshDeferred = that._emailDialog.refresh();
            refreshDeferred.then(dojo.hitch({
                'recepientName': recepientName,
                'senderName': senderName,
                'recepientEmail': recepientEmail
            }, function() {
                dojo.byId('id_recepient_name').value = this.recepientName;
                dojo.byId('id_sender_name').value = this.senderName;
                dojo.byId('id_recepient_email').value = this.recepientEmail;

                showEmailError(msg);
            }));
            btnSubmitEmail.disabled = false;
            btnSubmitEmail.innerHTML = 'Submit';
        }

        function showEmailError(msg)
        {
            var notificationEle = dojo.query('#email_error_notification p')[0];
            notificationEle.innerHTML = msg;
            dojo.style(dojo.query('#email_error_notification')[0], 'display', 'block');
        }
        
		var xhrArgs = {
			url: this.SHOCKCOLLAR_DOMAIN + '/key/register',
            form: dojo.query('form', this._emailDialog.id)[0],
			handleAs: 'text',
			load: function(response) {
                var responseObj = {};

                try
                {
                    responseObj = JSON.parse(response);
                }
                catch (err)
                {
                    serverError(that.MSG_EMAIL_ERROR);
                }

				if (responseObj.success == true) {
                    try
                    {
                        that._emailDialog.hide();
                    }
                    catch(err) { }

                    // save the instance key the server gave us
                    that._instanceKey = responseObj.data;

                    // Make sure to update the email field
                    that._tbEmail.set('value', dojo.byId('id_recepient_email').value);
                    that._beforeSaveDeferred.callback();
				} else {
                    serverError(responseObj.data.join('<br>'));
				}
			},
			error: function(error) {
                serverError(that.MSG_SERVER_ERROR);
			}
		};
		dojo.xhrPost(xhrArgs);
    },

    _submitFacebookInfo: function() 
    {
        /* Summary:
         *   Have the user login to facebook, let the response obj with the access token and fb id
         *   hit our client here, and immediately send to the server and wait for a successful 
         *   response.
         */  
		var that = this;
        console.log('in _submitFacebookInfo');
        showBlockingOverlay();

		FB.login(function(response) {
            if (response.authResponse)
            {
                console.log('response has authResponse');
                fbRegister(response.authResponse.userID, response.authResponse.accessToken);
            }
            else
            {
                serverError(that.MSG_FACEBOOK_INCORRECT);
            }
		}, { scope:'publish_stream, offline_access'} ); // user_online_presence

        function serverError(msg)
        {
            hideBlockingOverlay();
            that.showError(msg);
        }

        function showBlockingOverlay()
        {
            dojo.style(dojo.byId('screenBlocker'), 'display', 'block');
        }

        function hideBlockingOverlay()
        {
            dojo.style(dojo.byId('screenBlocker'), 'display', 'none');
        }

        function fbRegister(fbId, accessToken)
        {
            console.log('In FB register!');
            var xhrArgs = {
                url: that.SHOCKCOLLAR_DOMAIN + '/facebook/register',
                content: {
                    'fb_id': fbId,
                    'access_token': accessToken
                },
                load: function(response)
                {
                    hideBlockingOverlay();
                    console.log('server response fb_submit' + response);

                    var responseObj = {};
                    try
                    {
                        responseObj = JSON.parse(response);
                    }
                    catch (err)
                    {
                        responseObj.success = false;
                    }

                    if (responseObj.success === true)
                    {
                        that._instanceKey = responseObj.data;
                        that._fbId = fbId;
                        that._beforeSaveDeferred.callback();
                    }
                    else
                    {
                        serverError(that.MSG_FACEBOOK_REG_ERROR);
                    }
                },
                error: function(response)
                {
                    serverError(that.MSG_SERVER_ERROR);
                }
            };

            dojo.xhrPost(xhrArgs);
        }

    },

    validate: function(next) {
        /* Summary:
         * virtual.
         *  Check if an option is selected and that the monetary
         *  value for the paypal option is valid.
         */

        if (typeof this._selectedItem == 'undefined' 
            || this._selectedItem === null)
        {
            this.invalidMsg = "You must select an option to proceed.";
            return false;
        }
        else if (this._selectedItem == 'currency')
        {
            if (!this._tbCurrencyAmount.validate())
            {
                this.invalidMsg = 'Invalid currency amount. Must be at least '
                                    + '$1.00 and less than a ridiculous amount.';
                return false;
            }
        } 
        else if (this._selectedItem == 'password')
        {
            // Most of this validation is done in the email form
        }

        return true;
    },

    load: function() {
        var loaded = this.inherited(arguments, [[this.PANEL_NAME]]);
        var that = this;

        loaded.then(dojo.hitch(this, function()
        {
            try
            {
                var loadedInfo = JSON.parse(window.ShockCollar[that.PANEL_NAME]);
                that._selectedItem = loadedInfo.selectedDeterrent;
                that._currencyAmount = loadedInfo.currencyAmount;
                that._email = loadedInfo.email;
            }
            catch (err)
            {
                console.log('Could not load Deterrent info: ' + err);
            }

        }));

        return loaded;
    },

    save: function()
    {
        this.inherited(arguments);
        var that = this;

		var saveDeferred = new dojo.Deferred();

        var compiledDeterrentInfo = {
            selectedDeterrent: that._selectedItem,
            currencyAmount: that._tbCurrencyAmount.get('value'),
            email: that._tbEmail.get('value'),
            fbId: that._fbId,
            accessToken: that._accessToken,
            instanceKey: that._instanceKey
        };

        window.ShockCollar['Compiled' + that.PANEL_NAME] = JSON.stringify(compiledDeterrentInfo);

        that.sendSaveTrigger(that.PANEL_NAME);
        
        // Done saving at this point
        saveDeferred.callback();

        return saveDeferred;
    }

    /*_sendFBInfoToServer: function(userData)
    {
		var that = this;
		
		var xhrArgs = {
			url: 'http://idderall.com/facebook/register/',
			content: userData,
			handleAs: 'text',
			load: function(serverResponse) {
				if (serverResponse != '0') {

				} else {

				}
			},
			error: function(error) {

			}
		}
		deferred = dojo.xhrPost(xhrArgs);
    }*/

});
})();

