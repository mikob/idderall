(function(){
dojo.provide('shockcollar.panels.TermsAndConditions');

dojo.require('shockcollar.panels.Panels');

dojo.declare('shockcollar.panels.TermsAndConditions', [shockcollar.panels.Panels], {

	templateString: dojo.cache('shockcollar.panels', 'templates/TermsAndConditions.html'),

    PANEL_NAME: 'TermsAndConditions',
    CB_AGREE_ID: 'agreeToTerms',

    _agreed: false,

	constructor: function() {

	},
	
	postCreate: function() {
		this.inherited(arguments);
	},

	startup: function() {
		this.inherited(arguments);

        // We might have loaded a previous value into _agree
        dojo.byId(this.CB_AGREE_ID).checked = this._agreed;
	},

    validate: function() 
    {
        /* Summary:
         * virtual.
         *  Check if an option is selected and that the monetary
         *  value for the paypal option is valid.
         */
        var agreed = dojo.byId(this.CB_AGREE_ID).checked;
        if (agreed == false)
        {
            this.invalidMsg = "You must agree to the Terms and Conditions before proceeding.";
            return false;
        }

        return true;
    },

    load: function() {
        var loaded = this.inherited(arguments, [[this.PANEL_NAME]]);
        var that = this;

        loaded.then(dojo.hitch(this, function()
        {
            try
            {
                var loadedInfo = JSON.parse(window.ShockCollar[that.PANEL_NAME]);
                that._agreed = loadedInfo.agreed;
            }
            catch (err)
            {
                console.log('Could not load Terms and conditions info: ' + err);
            }

        }));

        return loaded;
    },

    save: function()
    {
        this.inherited(arguments);

		var saveDeferred = new dojo.Deferred();

        var agreed = dojo.byId(this.CB_AGREE_ID).checked;
		var compiledTermsAndCondsInfo = {
            'agreed': agreed,
        };

        window.ShockCollar['Compiled' + this.PANEL_NAME] = JSON.stringify(compiledTermsAndCondsInfo);

        this.sendSaveTrigger(this.PANEL_NAME);
        
        // Done saving at this point
        saveDeferred.callback();
    }

});
})();

