/* 
 * This content script is attached to the main menu and allows the user to
 * access various settings and see current status
 */

$(document).ready(function(){
    // Main menu
    $('#btnChangeSettings').click(function() {
        self.port.emit('ChangeSettings', null);
    });

    $('#btnAddToBlockList').click(function() {
        self.port.emit('LoadAddToBlocklist');

        $('#content div.Main').css('display', 'none');
        $('#content div.AddToBlockList').css('display', 'block');
    });

    $('#btnQuickBlock').click(function() {
        self.port.emit('LoadQuickBlock', null);

        $('#content div.Main').css('display', 'none');
        $('#content div.QuickBlock').css('display', 'block');
    });

    $('#content div.Setup .btnContinueSetup').click(function() {
        self.port.emit('OpenSetup');
    });

    $('#content div.AddToBlockList .btnCancel').click(function() {
        try
        {
            self.port.removeListener('LoadPanelData', loadPanelData);;
        } catch (err) { }

        self.port.emit('Hide');

        $('#content div.Main').css('display', 'block');
        $('#content div.AddToBlockList').css('display', 'none');
    });

    // For the sake of separation of content
    $('#content div.QuickBlock .btnCancel').click(function() {
        try
        {
            self.port.removeListener('LoadPanelData', loadPanelData);;
        } catch (err) { }

        self.port.emit('Hide');

        $('#content div.Main').css('display', 'block');
        $('#content div.QuickBlock').css('display', 'none');
    });

    var startHandler = function(newBlockScheduleData)
    {
        /* TODO: fix workaround related to this in AddToBlockList.js/Tree.js
         */
        console.log('Start clicked');
        var payload = newBlockScheduleData;
        var source = 'BlockSchedule';
        self.port.emit('SaveSettings', [source, payload]);
        self.port.emit('Hide');

        $('#content div.Main').css('display', 'block');
        $('#content div.QuickBlock').css('display', 'none');
    };

    var addHandler = function(newBlockListStoreData)
    {
        /* TODO: fix workaround related to this in AddToBlockList.js/Tree.js
         */
        console.log('Add clicked');
        var payload = newBlockListStoreData;
        var source = 'BlockLists';
        self.port.emit('SaveSettings', [source, payload]);
        self.port.emit('Hide');

        $('#content div.Main').css('display', 'block');
        $('#content div.AddToBlockList').css('display', 'none');
    };
    
    function loadSettings(payload)
    {
        var blockList = payload[0];
        var blockSchedule = payload[1];
        var domain = payload[2];

        unsafeWindow.ShockCollar = {
            'blockList': blockList,
            'blockSchedule': blockSchedule,
            'domain': domain,
            'addHandler': addHandler,
            'startHandler': startHandler
        };

        // send a trigger to startup
        var element = document.createElement('ShockCollarDataElement');
        document.body.appendChild(element);
        var evt = document.createEvent('Events');
        evt.initEvent('ShockCollar.LoadSettings', true, false);
        element.dispatchEvent(evt);
    }

    function showContinueSetup()
    {
        $('#content div.Main').css('display', 'none');
        $('#content div.Setup').css('display', 'block');
    }

    function showMain()
    {
        $('#content div.Main').css('display', 'block');
        $('#content div.Setup').css('display', 'none');
    }

    function hide()
    {
        // $('#content div.Setup').css('display', 'none');
        $('#content div.AddToBlockList').css('display', 'none');
        $('#content div.QuickBlock').css('display', 'none');
    }

    self.port.on('Hide', hide);
    self.port.on('LoadSettings', loadSettings);
    self.port.on('ShowMain', showMain);
    self.port.on('ShowContinueSetup', showContinueSetup);
});
