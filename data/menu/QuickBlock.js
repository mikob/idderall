dojo.ready(function() {
dojo.require('dijit.form.ComboBox');
dojo.require('dojo.data.ItemFileWriteStore');
dojo.require('dijit.form.DateTextBox');
dojo.require('dijit.form.TimeTextBox');

dojo.ready(function() {
    // consruction args
    var blockSchedule = null;

    // ...Handlers
    var startHandler = null;

    // dojo attach points
    var _groupSelect = null;
    var _endDate = null;
    var _endTime = null;
    var _startTime = null;

    document.addEventListener('ShockCollar.LoadSettings', function() {
        startup(window.ShockCollar.blockSchedule);
    });

    function startup(_blockSchedule)
    {
        /* Summary:
         *  Anything that needs data that is loaded in needs to be called in 
         *  this startup() function or later. For example the block lists
         *  page needs to load the blocklists before it can show the block tree
         *  so the block tree needs to be initialized in startup so it can have
         *  the loaded data.
         */
        blockSchedule = JSON.parse(_blockSchedule);
        var groups = [];
        for (group in blockSchedule)
        {
            groups.push(group);
        }

        var options = new dojo.data.ItemFileWriteStore({data: {
            identifier: 'name', 
            items: []
        }});

        for (var i = 0; i < groups.length; i++)
        {
            options.newItem({name: groups[i], value: groups[i]});
        }
        dijit.byId('groupSelect').attr('store', options);

        // Populate default values
        dijit.byId('groupSelect').set('value', 'Group 1');
        dijit.byId('startTime').set('value', new Date());
        dijit.byId('endTime').set('value', new Date());
        dijit.byId('endDate').set('value', new Date());

        dojo.connect(dojo.query('#content div.QuickBlock .btnStart')[0], 'onclick', startBlock);
    }

    function startBlock(evt)
    {
        /* Summary:
         *  Using the given block schedule, append a tempBlock object
         *  with timeStart and timeEnd properties.
         */
        dojo.stopEvent(evt);

        // First we need to validate the parameters
        // Now we can compile the new blockSchedule with the ammendment
        var newBlockSchedule = blockSchedule;
        var group = dijit.byId('groupSelect').get('value');
        
        // Lets make a javascript date object out of the start
        // parameters
        var startDateTime = new Date();
        var endDateTime = new Date();
        var tempStart = endDateTime;

        startDateTime.setHours(dijit.byId('startTime').get('value').getHours());
        endDateTime.setHours(dijit.byId('endTime').get('value').getHours());
        
        startDateTime.setMinutes(dijit.byId('startTime').get('value').getMinutes());
        endDateTime.setMinutes(dijit.byId('endTime').get('value').getMinutes());

        startDateTime.setSeconds(0);
        endDateTime.setSeconds(0);

        newBlockSchedule[group].tempBlock = {};
        newBlockSchedule[group].tempBlock.timeStart = startDateTime;
        newBlockSchedule[group].tempBlock.timeEnd = endDateTime;

        window.ShockCollar.startHandler(JSON.stringify(newBlockSchedule));
    }

});
});
