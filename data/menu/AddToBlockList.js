dojo.ready(function() {
dojo.require('dojo.data.ItemFileWriteStore');
dojo.require('dijit.tree.ForestStoreModel');
dojo.require('dijit.form.CheckBox');
dojo.require('dijit.form.TextBox');

dojo.require('shockcollar.widgets.Tree');

dojo.ready(function(){
    // Constants
	var TREE_WIDTH = '232px';
	var TREE_HEIGHT = '200px';
    var MIN_TREE_SIZE = 100;

    var blockListStore = null;
    var blockListTreeModel = null;
    var blockListTree = null;

    document.addEventListener('ShockCollar.LoadSettings', function() {
        startup(window.ShockCollar.blockList, window.ShockCollar.domain);
    });

    function startup(store, domain)
    {
        /* Summary:
         *  Anything that needs data that is loaded in needs to be called in 
         *  this startup() function or later. For example the block lists
         *  page needs to load the blocklists before it can show the block tree
         *  so the block tree needs to be initialized in startup so it can have
         *  the loaded data.
         */
        
        // Load the store from the previous block list store
        if (store == null)
        {
            console.log('Could not parse CompiledBlockLists');
        }
        else 
        {
            store = JSON.parse(store);
        }

        // store = {
        //         "label": "name",
        //         "items": [ 
        //             { name: "Group 1", type: "group", children: [
        //                 { name: "reddit.com", type:"individual", domain:"*.reddit.com" },
        //                 { name: "digg.com", type:"individual", domain: "*.digg.com" },
        //                 { name: "facebook.com", type: "individual", domain: "*.facebook.com" }
        //                 ] }
        //             ] 
        //     };
        blockListStore = new dojo.data.ItemFileWriteStore({
            data: store
        });

        blockListTreeModel = new dijit.tree.ForestStoreModel({
            store: blockListStore,
            childrenAttrs: ['children']
        });

        if (blockListTree)
        {
            blockListTree.destroy();
        }

		blockListTree = new shockcollar.widgets.Tree({
			model: blockListTreeModel,
			minSize: '50px',
			style: 'width:' + TREE_WIDTH + ';height:' + TREE_HEIGHT + ';',
            CSS_SHEET_NAME: "Menu.css"
		});

		blockListTree.placeAt(dojo.byId('blockListTree'));

        // Populate domain text box
        if (domain != null)
        {
            try
            {
                domain = domain.replace('www.', '');
            }
            catch(err)
            {

            }
            dijit.byId('domainInput').set('value', domain);
        }

        dojo.connect(dojo.query('#content div.AddToBlockList .btnAdd')[0], 'onclick', this, _addToSelectedBlockList);

        blockListTree.startup();
    }

    function _addToSelectedBlockList(evt)
    {
        /* Summary:
         *  This is what is called when the domain we have needs to be added 
         *  to the actual block list store. This funciton is in charge of
         *  calling routines to store the new domain in memory. It will
         *  also need to call the addHandler we have from the Content Script
         */
        dojo.stopEvent(evt);

        // First we add to the selected groups
		var siteName = dijit.byId('domainInput').get('value');
        var domain = '*.' + siteName;

		if (siteName)
		{
            // TODO: allow multiple selection
			var group = blockListTree.getSelectedGroup(/*defaultToFirst*/false, true);

			// Add the domain, the group should be set by now
			if (group)
			{
				blockListTree.addItemToTree(group, siteName, 'individual', domain);
			}
		}

        // Now we can make a brand new store with all of our data
        // TODO: WHY DOESN't THIS WORK LIKE IT DOES IN BLOCKLISTS.JS, a workaround
        // is implemented here
        blockListTree.makeStoreData(function(data)
        {
            console.log('IN MAKE STORE DATA: ' + data);
            window.ShockCollar.addHandler(JSON.stringify(data));
        });

    }


});
});
