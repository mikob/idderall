/* This content script file gets attached to the pages that tells
 * a user a site is blocked.
 */

var origUrl;

self.port.on('BlockRelease', function()
{
    window.location.href = origUrl;

    // send msg to destroy self after we refresh
    $(document).ready(function() {
        self.port.emit('Destroy');
    });
});

self.port.on('Load', function(payload)
{
    console.log('In Block Page Load');

    if (payload)
    {

        var msg = payload.msg;
        var cheatMsg = payload.cheatMsg;

        try
        {
            // Use index of to make sure we don't slice out '&url=' from the url
            // itself
            origUrl = decodeURIComponent(unescape(window.location.href.split('#url=')[1]));
        }
        catch(e)
        {
            origUrl = window.location.href;
            console.log(e);
        }

        $(document).ready(function() {
            var msgEle = $('#message')[0];
            msgEle.textContent = msg;

            if (cheatMsg) {
                var cheatMsgEle = $('#cheatMessage')[0];
                cheatMsgEle.textContent = cheatMsg;
            }

            $('#_btnGiveUp').click(function() {
                if (confirm("Are you sure you want to do this?"))
                {
                    // Make them suffer the appropriate deterrent here
                    self.port.emit('GiveUp');
                }

            });

            // Refresh button does not work in add on Content script as of
            // add on sdk 1.4.3
            
        });
    }
});


