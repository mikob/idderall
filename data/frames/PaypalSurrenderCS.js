/* 
 * Load in the amount the user chose when this page loads
 */

var checkDelay = 10000;
var checked = 0;

self.port.on('Load', function(payload)
{
    var currencyAmount = payload.currencyAmount;
    var instanceKey = payload.instanceKey;

    unsafeWindow.onbeforeunload = function () {
        alert("Leave this page open until payment is received and "
                + "blocks are released.");
        return false;
    };
    
    $(document).ready(function() {
        $('#amount').val(currencyAmount);
        $('#item_number').val(instanceKey);

        $('#checkBtn').click((function() {
            checkIfPayed(instanceKey);
        }).bind(this));

        waitAndCheck(instanceKey);
    });
});

function waitAndCheck(instanceKey) {
    setTimeout(function() {
        checked += 1;
        // every ten checks, double the check delay
        checkDelay *= Math.floor(checked/10) + 1;
        checkIfPayed(instanceKey);
    }, checkDelay);
}

function checkIfPayed(instanceKey)
{
    $.get("http://idderall.com/paypal/status", { 'instance_key': instanceKey })
    .success(function(response) {
        console.log('success with paypal surrender page: ' + response);
        if (response == 'Success')
        {
            unsafeWindow.onbeforeunload = null;
            console.log('emit unblock');
            self.port.emit('Unblock');
        }
        else 
        {
            // Keep checking
            waitAndCheck(instanceKey);
        }
    });
}
