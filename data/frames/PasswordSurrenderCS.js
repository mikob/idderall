/* This content script file gets attached to the pages that tells
 * a user a site is blocked.
 */

self.port.on('Load', function(instanceKey) {

    console.log('password surrender instance key: ' + instanceKey);
    $(document).ready(function() {
        $('#btnUnblock').click(function() {
            
            // Make them suffer the appropriate deterrent here
            var password = $('#tbKey').val();

            $.get("http://idderall.com/key/validate", { 'instance_key': instanceKey, 'password': password })
            .success(function(response) {
                console.log('success with key: ' + response);
                if (response == 'Success')
                {
                    self.port.emit('Unblock');
                }
                else 
                {
                    var notification = $('.notification');
                    notification.css('display', 'block');
                    $('.notification p')[0].innerHTML = 'Invalid key.';
                }
            })
            .error(function(response) {
                console.log('error with key: ' + response);
                var notification = $('.notification');
                notification.css('display', 'block');
                $('.notification p')[0].innerHTML = 'Server error. Please contact '
                    + 'support@idderall.com if problem persists';
            });

        });
    });

});

