
const REMOTE_DOMAIN = "http://idderall.com";
const REMOTE_SECURE_DOMAIN = "https://idderall.com";
const REMOTE_DEBUG_DOMAIN = "http://127.0.0.1:8000";

exports.remoteFile = function(secure, path)
{
    if (secure)
    {
        return REMOTE_SECURE_DOMAIN + '/static/' + path;
    }
    else
    {
        return REMOTE_DOMAIN + '/static/' + path;
    }
};

/* Summary:
 *  Puts zero padding on the given number.
 *
 * Assumptions:
 *  Not a decimal number.
 *
 * returns [String]:
 *  The padded string.
 * 
 * val [Number, String]:
 *  The number that needs to have zeros padded
 *  onto it.
 *
 * padSize [padSize]:
 *  The total size in chars the formatted number 
 *  should be. 
 *  ex. "1" val with padSize = 2, returns "01"
 */
exports.padZeroes = function(val, padSize)
{
    var num = Number(val);
    var str = String(Math.abs(num));
    if (padSize > str.length)
    {
        var zeroesStr = (0).toFixed(padSize - str.length).split('.')[1];

        if (num < 0) 
        {
            zeroesStr = ('-').concat(zeroesStr);
        }
        return zeroesStr.concat(str);
    }
    else 
    {
        return String(val);
    }
};

/* Summary:
 *  Return a js date with the time specified in the parameter.
 */
exports.strToDate = function(timeString)
{
    var result = new Date();
    var colonSplit = timeString.split(':');

    result.setHours(colonSplit[0]);
    result.setMinutes(colonSplit[1]);

    // don't want dependence on the seconds of when new Date() is called,
    // rather, happen exactly when the minute strikes
    result.setSeconds(0);

    return result;
};

exports.formatDateTime = function(type, date)
{
    var formattedStr;

    if (type == 'time')
    {
        formattedStr = _formatTime(date);
    }
    else if (type == 'datetime')
    {
        formattedStr = _formatDate(date) + ' ' + _formatTime(date);
    }

    return formattedStr;
};

function _formatDate(date)
{
    return date.toDateString();
}

function _formatTime(date)
{
    var hour = date.getHours();
    var minutes = date.getMinutes();
    var ampm;

    if (minutes < 10) 
    {
        minutes = '0' + String(minutes);
    }

    if (hour < 12)
    {
        ampm = 'am';
    }
    else
    {
        ampm = 'pm';
    }

    hour %= 12;
    if (hour == 0) {
        hour = 12;
    }

    return hour + ':' + minutes + ' ' + ampm;
}
