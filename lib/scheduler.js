/* 
 * scheduler.js
 *
 *  This module allows scheduling of future events. The user can specify for
 * certain callbacks when an event time is hit. Scheduled events are not 
 * persistent over browser restarts. They are stored in temporary memory. The
 * timer does not run when there are no scheduled events. Scheduled events must
 * be destroyed manually if an event is cancelled before a callback.
 */
const Debug = require('debug');

const Timer = require('timers');

exports.addEvent = addEvent;
exports.addEventWithUpdate = addEventWithUpdate;
exports.removeEvent = removeEvent;

var _schedule = [];
var _timerID = null;
var _timerLoopRunning = false;
var _lastID = 0;
var _updateCallbacks = [];


/* Summary:
 *  Add an event to the scheduler and start the timer tick and loop if
 *  it's not already started.
 *
 * returns [Number]:
 *  An event id, unique for each event (not correlated to the index) which
 *  can be used to remove the event later before it's called back.
 */
function addEvent(/* datetime */ time, /* function */ callback)
{
    console.log('in scheduler add event');
    var eventID = _getNextEventID();
    _schedule.push({ 
        'time': time, 
        'callback': callback,
        'id': eventID
    });

    // Run the timer if it's not already running
    if (!_timerLoopRunning)
    {
        _timerID = Timer.setInterval(_tick, 1000);
        _timerLoopRunning = true;
    }

    return eventID;
}

/* Summary:
 *  Does the same addEvent does, but also calls updateCallback on each tick
 */
function addEventWithUpdate(/* datetime */ time, /* function */ endCallback, 
        /* function */ updateCallback)
{
    console.log('in scheduler add event with update');
    var eventID = addEvent(time, endCallback);
    _updateCallbacks.push({
        'id': eventID,
        'callback': updateCallback
    });

    return eventID;
}

/* Summary:
 *  Removes an event from the scheduler and stops the timer if that was
 *  the last event. If the event was not found to exist, a debug error
 *  is thrown.
 *
 * eventID [String]:
 *  The id of the event to remove (the id is returned when an event is
 *  created, and is not the index in the array)
 */
function removeEvent(/* String */ eventID)
{
    var removed = false;

    console.log('in remove event. EventID: ' + eventID);
    for (var i = 0; i < _schedule.length; i++)
    {
        if (_schedule[i].id === eventID)
        {
            _schedule.splice(i, 1);
            removed = true;
        }
    }

    for (var k = 0; k < _updateCallbacks.length; k++)
    {
        console.log('removing ' + _updateCallbacks.length + ' update callbacks');
        if (_updateCallbacks[k].id === eventID)
        {
            _updateCallbacks.splice(k, 1);
        }
    }

    if (!removed)
    {
        Debug.warn("Event not found for removal");
    }

    // Stop ticking the timer if we removed the last event
    if (_schedule.length === 0)
    {
        Timer.clearInterval(_timerID);
        _timerLoopRunning = false;
    }
}

/* Summary:
 *  Gets the next available unique id.
 * 
 * Return [Number]:
 *  The next available unique id for use. 
 */
function _getNextEventID()
{
    _lastID = _lastID + 1;
    return _lastID;
}

/* Summary:
 *  Internal clock tick loop. Stops the interval that activated it if the
 *  schedule is empty. Checks if an event needs to occur, and performs the
 *  callback while removing the event from the schedule.
 */
function _tick()
{
    var now = new Date();

    for (var j = 0; j < _updateCallbacks.length; j++)
    {
        _updateCallbacks[j].callback();
    }

    for (var i = 0; i < _schedule.length; i++)
    {
        // If the event occurs now
        if (_schedule[i].time <= now)
        {
            _schedule[i].callback();

            // Remove it from the scheduler
            removeEvent(_schedule[i].id);
        }
    }

    // Stop ticking by itself if we notice the schedule is empty
    if (_schedule.length === 0)
    {
        Timer.clearInterval(_timerID);
        _timerLoopRunning = false;
    }
}

