var profile = {
    releaseDir: "/home/miko/workspace/ShockCollar/data/dojo",
    basePath: ".",
    action: "release",
    cssOptimize: "comments",
    mini: false,
    optimize: "closure",
    layerOptimize: "closure",
    selectorEngine: "acme",

    layers: {
        "dojo/dojo": {
            include: ["dojo/dojo", "shockcollar"],
            customBase: true,
            boot: true
        }
    },
