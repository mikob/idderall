/* 
 * Simple wrapper for notifying the user with messages.
 */
const Data = require('self').data;
const Notifications = require('notifications');

const NOTIFICATION_ICON_URL = Data.url('media/logox64.png');

exports.msg = function(msg) {
    Notifications.notify({
        title: 'Idderall',
        text: msg,
        iconURL: NOTIFICATION_ICON_URL
    });
};
