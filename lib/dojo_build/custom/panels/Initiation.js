(function(){
dojo.provide('shockcollar.panels.Initiation');

dojo.require('shockcollar.panels.Panels');

dojo.declare('shockcollar.panels.Initiation', [shockcollar.panels.Panels], {
	templateString: dojo.cache('shockcollar.panels', 'templates/Initiation.html'),

	constructor: function() {

	},
	
	postCreate: function() {
		this.inherited(arguments);
	},

	startup: function() {
		this.inherited(arguments);
	}


});
})();

