(function(){
dojo.provide('shockcollar.panels.BlockLists');

dojo.require('dojo.data.ItemFileWriteStore');
dojo.require('dijit.tree.ForestStoreModel');
dojo.require('dijit.tree.dndSource');
dojo.require('dijit.form.TextBox');
dojo.require('dijit.Tooltip');
dojo.require('dijit.form.CheckBox');
dojo.require('dijit.form.NumberSpinner');

dojo.require('shockcollar.widgets.Tree');
dojo.require('shockcollar.panels.Panels');

dojo.declare('shockcollar.panels.BlockLists', [shockcollar.panels.Panels], {
	//consts
    PANEL_NAME: "BlockLists",
	NEW_GROUP_FOLDER_NAME: 'Add items here to create new group...',
	CONFIRM_DELETE_GROUP: 'Are you sure you would like to delete',
	DOMAIN_HINT: 'domain (ie. google.com)',
	DEFAULT_CHEAT_MODE_MINS: 5,
	TREE_WIDTH: '260px',
	TREE_HEIGHT: '320px',

	templateString: dojo.cache('shockcollar.panels', 'templates/BlockLists.html'),

	lastID: null,
	numGroups: 1, 		// used to name the next group 
    blockListStoreData: null,   //save, load this store info
	blockListStore: null,
	commonListStore: null,
	blockListTreeModel: null,
	commonListTreeModel: null,
	blockListTree: null, 	// The tree where the user puts the blocked sites on
	commonListTree: null, 	// The tree where the users can drag common sites into their blocklists from

	currentBlockListGroupID: null, // The id of the selected block list group
	savedGroupSettings: null,

    // number of items in the block list, needs to be at least 1 to validate
    _numBlockItems: null, 

	constructor: function() {
		this.inherited(arguments);
		
		// Initialize array here so it is not static
		this.savedGroupSettings = [];
	},
	
	postCreate: function() {
		this.inherited(arguments);

		var that = this;

		var commonListStoreData = { 
			"label": "name",
			"items": [
                { name: "Entertainment Sites", type: "group", children: [
                    { name: "youtube.com", type: "individual", domain: "*.youtube.com" },
                    { name: "break.com", type: "individual", domain: "*.break.com" },
                    { name: "addictinggames.com", type: "individual", domain: "*.addictinggames.com" }
                ] },
				{ name: "Social News Sites", type: "group", children: [
					{ name: "reddit.com", type: "individual", domain: "*.reddit.com" },
					{ name: "digg.com", type: "individual", domain: "*.digg.com" },
					{ name: "delicious.com", type: "individual", domain: "*.delicious.com" },
					{ name: "fark.com", type: "individual", domain: "*.fark.com" },
					{ name: "stumbleupon.com", type: "individual", domain: "*.stumbleupon.com" },
					{ name: "slashdot.org", type: "individual", domain: "*.slashdot.com" },
					{ name: "newsvine.com", type: "individual", domain: "*.newsvine.com" }
                ] },
				{ name: "Social Networking Sites", type: "group", children: [
					{ name: "facebook.com", type: "individual", domain: "*.facebook.com" },
					{ name: "twitter.com", type: "individual", domain: "*.twitter.com" },
					{ name: "linkedin.com", type: "individual", domain: "*.linkedin.com" },
					{ name: "myspace.com", type: "individual", domain: "*.myspace.com" },
					{ name: "ning.com", type: "individual", domain: "*.ning.com" },
					{ name: "plus.google.com", type: "individual", domain: "*.plus.google.com" },
					{ name: "tagged.com", type: "individual", domain: "*.tagged.com" }
                ] }
				]
		};

		this.commonListStore = new dojo.data.ItemFileReadStore({
			data: commonListStoreData
		});

		this.commonListTreeModel = new dijit.tree.ForestStoreModel({
			store: this.commonListStore,
			childrenAttrs: ['children']
		});

	},

	startup: function() {
		this.inherited(arguments);
		var that = this;

        var fresh = false;

        // Check to make sure it hasn't been loaded already
        if (typeof this.blockListStoreData == 'undefined' 
            || this.blockListStoreData == null || !this.blockListStoreData.items)
        {
            this.blockListStoreData = {
                "label": "name",
                "items": [ 
                        { name: this.NEW_GROUP_FOLDER_NAME, type: 'group',
                            newGroup: true, children: [] }
                ] 
            };

            fresh = true;
        }

		this.blockListStore = new dojo.data.ItemFileWriteStore({
			data: this.blockListStoreData
		});

        // If we are starting a fresh list (can happen from null data, new user, etc)
        if (fresh)
        {
            // this needs to be done for the newGroup manually, since we aren't calling
            // the createNewGroup method
            this.blockListStore.fetch({
                query: { 
                    'type': 'group',
                    'newGroup': true
                },
                onItem: function(item) {
                    that.lastID = that.blockListStore.getIdentity(item);
                }
            });
        }

		this.blockListTreeModel = new dijit.tree.ForestStoreModel({
			store: this.blockListStore,
			childrenAttrs: ['children']
		});
		
		this.blockListTree = new shockcollar.widgets.Tree({
			model: that.blockListTreeModel,
			checkItemAcceptance: dojo.hitch(that, that.dndCheckDrop),
			checkAcceptance: dojo.hitch(that, that.dndCheckDrag),
			minSize: that.MIN_TREE_SIZE,
			style: 'width:' + that.TREE_WIDTH + ';height:' + that.TREE_HEIGHT + ';',
			persist: true 		// Cookie saving
		});
			
		var blockListDndCtrl = new dijit.tree.dndSource(this.blockListTree, {
			accept: ['individual'],
			itemCreator: dojo.hitch(that, that.blockListDndDrop),
			checkItemAcceptance: dojo.hitch(that, that.dndCheckDrop),
			checkAcceptance: dojo.hitch(that, that.dndCheckDrag)
		});

		this.blockListTree.placeAt(this._userBlockLists);

		this.commonListTree = new shockcollar.widgets.Tree({
			model: that.commonListTreeModel,
			minSize: that.MIN_TREE_SIZE,
			style: "width:" + that.TREE_WIDTH + ";height:" + that.TREE_HEIGHT + ";"
		});

		var commonListDndCtrl = new dijit.tree.dndSource(this.commonListTree, {
			copyOnly: true,
			isSource: true,
			checkItemAcceptance: dojo.hitch(that, that.dndCheckDrop),
			checkAcceptance: dojo.hitch(that, that.dndCheckDrag)
		});

		this.commonListTree.placeAt(this._commonBlockLists);


		// connect the on new item function to the blockList
		// so that new folder are added when dragged to the new
		// folder group
		this._connections.push(dojo.connect(that.blockListTreeModel, 
			'onNewItem', dojo.hitch(that, that.itemAddedToBlockList)));

		this._connections.push(dojo.connect(this._leftBtn, 'onclick', dojo.hitch(this, this.removeDomainsOrGroupFromBlockList)));

		this._connections.push(dojo.connect(this._rightBtn, 'onclick', dojo.hitch(this, this.copyDomainsFromCommonList)));

		this._connections.push(dojo.connect(dojo.byId('addSiteBtn'), 'onclick', dojo.hitch(this, this.addSite)));

		this._connections.push(dojo.connect(this._domainInput, 'onKeyUp', dojo.hitch(this, this.checkForEnterKey)));

		this._connections.push(dojo.connect(this._domainInput, 'onFocus', dojo.hitch(this, this.hideDomainHint)));

		this._connections.push(dojo.connect(this._domainInput, 'onBlur', dojo.hitch(this, this.showDomainHint)));

		this._connections.push(dojo.connect(this.blockListTree, 'onClick', dojo.hitch(this, this.treeSelectionChanged)));

		this._connections.push(dojo.connect(this.commonListTree, 'onClick', dojo.hitch(this, this.treeSelectionChanged)));

		new dijit.Tooltip({
			connectId: ['invertModeInput','invertModeInputLabel'],
			position: 'after',
			label: '<strong>NOT YET SUPPORTED</strong><br>\
                <i>Only allow</i> the sites in a block group.<br>\
				Disallow all domains outside what is specified in the group.'
		});

		new dijit.Tooltip({
			connectId: ['addSiteBtn'],
			position: 'before',
			label: 'Adds a domain to the <b>selected group</b> in the tree.'
		});

		new dijit.Tooltip({
			connectId: ['domainInput'],
			position: 'before',
			label: 'Domain of interest to block. Do not include: \"http://www.\"<br><br>\
						<i>examples:<br>\
						&nbsp;&nbsp;addictivesites.com<br>\
						&nbsp;&nbsp;reddit.com</i>'
		});

        if (!fresh)
        {
            this.createNewGroupFolder();
        }

        this.currentBlockListGroupID = this.blockListStore.getIdentity
                (this.blockListTree.getSelectedGroup(true, true));
            
		this.showDomainHint();
	},

	validate: function()
	{
		/* VIRTUAL
		 * Summary:
		 * 	* Make sure there is at least 1 block group
		 * 	with at least 1 block item
		 * 	* Make sure we're under the 5mb storage quota
		 */
        // WARNING WARNING WARNING USING A PRIVATE DOJO VAR HERE!!!
        if (this.blockListStore._arrayOfAllItems.length > 1)
        {
            return true;
        }
        else
        {
            this.invalidMsg = "You must add at least 1 item to \"Your Blocked Sites\" to proceed.";
            return false;
        }
	},

	save: function()
	{
		/* VIRTUAL
		 * Summary:
		 * 	Save any settings pertinent to this 
		 * 	panel.
         * Note: 
         *  Validation needs to be checked before this is called!
		 */

		// Save is async
        this.inherited(arguments);

		var saveDeferred = new dojo.Deferred();
        var that = this;

        var deferred = this.blockListTree.makeStoreData();

        deferred.then(function(value){
            window.ShockCollar.CompiledBlockLists = JSON.stringify(value);
            that.sendSaveTrigger(that.PANEL_NAME);

            // Done saving at this point
            saveDeferred.callback();
        });
		return saveDeferred;
	},

    load: function()
    {
        /* Summary:
         *  Loaded is an overriden virtual function from Panels.js. We simply
         *  pass the name of this panel type to the base method and a series
         *  of steps take place to load necessities from the add-on storage.
         *  The base virtual method returns a deferred object. The load for
         *  this panel in particular has a global js object "window.ShockCollar
         *  .CompiledBlockList" set after the loading is finished. This 
         *  object will contain the loaded data.
         */
        var that = this;
        var loaded = this.inherited(arguments, [[this.PANEL_NAME]]);

        // Any extra specific steps to take place after loading is done go in
        // this deferred.then function
        loaded.then(function() 
        {
            try
            {
                var storeStr = window.ShockCollar[that.PANEL_NAME];

                that.blockListStoreData = JSON.parse(storeStr);
            }
            catch (err)
            {
                console.log('Could not load blocklists: ' + err);
            }
        });

        return loaded;
    },

	blockListDndDrop: function(nodes, target, source)
	{
		/* Summary:
		 * 	Kind of a hack. Creates an object with the raw
		 * 	item data values from the "nodes" param which
		 * 	is the dragged in selection.
		 */
		var that = this;
		var items = [];

		dojo.forEach(nodes, function(node) 
		{
			var tempItem = {};
			var draggedItem = dijit.getEnclosingWidget(node).item;

			tempItem.name = that.commonListStore.getValue(draggedItem, 'name');
			tempItem.domain = that.commonListStore.getValue(draggedItem, 'domain');
			tempItem.type = that.commonListStore.getValue(draggedItem, 'type');

			items.push(tempItem);
		});

		return items;
	},

	treeSelectionChanged: function()
	{
		/* Summary:
		 * 	Enables/disables right and left movement buttons 
		 * 	depending on tree node selection.
		 * Cases:
		 * 	*If a node is selected on the blocked sites
		 * 	then enable the left button but not the right.
		 * 	*If a non-group node is selected on the commonly
		 * 	blocked sites ONLY, then only the right arrow is 
		 * 	enabled.
		 * 	*If there is a selection on both trees (and the 
		 * 	common sites tree is not only a group selection)
		 * 	enable both buttons
		 * 	*No selection, both buttons disabled
		 */

		//This might get called when the tree is not initialized and the
		//selectedItems return null
		try
		{
			if (this.blockListTree.get('selectedItems').length > 0 
			    && this.commonListTree.get('selectedItems').length > 0
			    && !this.commonListTree.getSelectedGroup(false, false)
			    && this.isNonNewGroupSelected())
			{
				dojo.attr(this._leftBtn, 'disabled', false);
				dojo.attr(this._rightBtn, 'disabled', false);
			}
			else if (this.blockListTree.get('selectedItems').length > 0
			    && (this.commonListTree.get('selectedItems').length == 0
				|| this.commonListTree.getSelectedGroup(false, false))
			    && this.isNonNewGroupSelected())
			{
				dojo.attr(this._leftBtn, 'disabled', false);
				dojo.attr(this._rightBtn, 'disabled', true);
			}
			else if ((this.blockListTree.get('selectedItems').length == 0
				   || !this.isNonNewGroupSelected())
			    && this.commonListTree.get('selectedItems').length > 0
			    && !this.commonListTree.getSelectedGroup(false, false))
			{
				dojo.attr(this._leftBtn, 'disabled', true);
				dojo.attr(this._rightBtn, 'disabled', false);
			}
			else
			{
				dojo.attr(this._leftBtn, 'disabled', true);
				dojo.attr(this._rightBtn, 'disabled', true);
			}
		}
		catch (e)
		{

		}
			
		this.updateGroupSettings();


	},

	getGroupSettings: function(group)
	{
		/* Summary:
		 * 	Get the group settings if they are stored,
		 * 	return defaults if not.
		 */
		var groupID = this.blockListStore.getIdentity(group);
		var savedSettings = this.savedGroupSettings[groupID];

		if (typeof savedSettings == 'undefined')
		{
			savedSettings = {};

			// Load default settings
			savedSettings.invertModeInput = false;
		}

		return savedSettings;
	},

	updateGroupSettings: function()
	{
		var newGroup = this.blockListTree.getSelectedGroup(true, true);
		var newGroupID = this.blockListStore.getIdentity(newGroup); 

		if (newGroupID != this.currentBlockListGroupID && this.isNonNewGroupSelected())
		{
			// Save the current settings before we change the current block
			// list group id
			this.saveCurrentGroupSettings(this.currentBlockListGroupID);

			// Change the block list group physically
			this.loadGroupSettings(newGroup);

			// Safe to change the block list id now
			this.currentBlockListGroupID = newGroupID;
		}
	},

	saveCurrentGroupSettings: function(id)
	{
		/* Summary:
		 * 	Save all the value parameters in the group
		 * 	settings related controls in a 'dictionary'
		 * 	(really an associative array in js)
		 * 	where the the key is the group id.
		 */

		this.savedGroupSettings[id] = {
			invertModeInput: this._invertModeInput.get('checked')
		};

	},

	loadGroupSettings: function(/*dojo data item*/ group)
	{
		/* Summary:
		 * 	Load the saved settings onto the sidebar 
		 * 	settings widget.
		 */
		var savedSettings = this.getGroupSettings(group);
		var groupName = this.blockListStore.getValue(group, 'name');
		var invertModeInput;

		// Title
		dojo.byId('groupSettings').innerHTML = groupName + ' Advanced Settings';

		// Load the saved settings
		invertModeInput = savedSettings.invertModeInput;

		// Set control values to saved settings
		this._invertModeInput.set('checked', invertModeInput);

	},

	isNonNewGroupSelected: function()
	{
		/* Summary:
		 * 	Checks the block list tree to make sure that the
		 * 	new group is not the only item selected.
		 * 	Returns false if only a new group is selected,
		 * 	true if other items are selected.
		 */
		var val = false;
		var that = this;

		try 
		{
			dojo.some(this.blockListTree.get('selectedItems'), function(item) 
			{
				// if the newGroup field is falsy (specifically undefined or false)
				if (!that.blockListStore.getValue(item, 'newGroup'))
				{
					val = true;

					//Break from the loop
					return false;
				}
			});
		}
		catch (e)
		{
		}

		return val;
	},

	removeDomainsOrGroupFromBlockList: function()
	{
		/* Summary:
		 * 	Removes domain items or whole groups from
		 * 	the block list tree depending on the users
		 * 	selection.
		 * 	Checks for empty groups and removes those
		 * 	as well.
		 * Note:
		 * 	Ask for confirmation from the user when
		 * 	removing a whole group of domains
		 */
		var that = this;
		var itemsToDelete = this.blockListTree.getSelectedItems(true);

		if (itemsToDelete.length > 0)
		{
			// The following line is a workaround for a dojo 1.6.0 bug
			// where things break when the selected item in a tree is
			// deleted. 
			// Remove everything from selection to prevent it...
			that.blockListTree.set('selectedNodes', []);

			dojo.forEach(itemsToDelete, function(item)
			{
				if (that.blockListStore.getValue(item, 'type') == 'individual')
				{
					// Put this in a try catch in case the items
					// group was already deleted (then this was 
					// also deleted)
					try
					{
						that.blockListStore.deleteItem(item);
					}
					catch (e)
					{

					}
				}
				// Make sure its not the "newGroup" template
				else if (that.blockListStore.getValue(item, 'type') == 'group'
					&& !that.blockListStore.getValue(item, 'newGroup'))
				{
					if (confirm(that.CONFIRM_DELETE_GROUP 
						+ ' ' 
						+ that.blockListStore.getValue(item, 'name') 
						+ '?'))
					{
						that.blockListStore.deleteItem(item);
					}
				}
						
			});

			this.trimGroups();

			// When finished groups could have been adjusted
			// so update the group names
			this.setGroupNames();

			// After trimming and updating names, we can 
			// check the changed tree selection
			this.treeSelectionChanged();
		}

	},

	trimGroups: function()
	{
		/* Summary:
		 * 	Remove the empty groups from the block list
		 */
		var that = this;
		
		this.blockListStore.fetch({
			query: { 'type': 'group' },
			onItem: function(item) {
				if (!that.blockListStore.getValue(item, 'newGroup')
				    && typeof that.blockListStore.getValue(item, 'children') == 'undefined'
				    || (typeof that.blockListStore.getValue(item, 'children') != 'undefined'
				    && that.blockListStore.getValue(item, 'children').length == 0))
				{
					that.blockListStore.deleteItem(item);
				}
			}
		});
	},

	checkForEnterKey: function(evt)
	{
		/* Summary:
		 * 	Checks if the enter key was pressed while the 
		 * 	domain input box is focused, to allow quickly
		 * 	adding domains.
		 */
		if (evt.keyCode == dojo.keys.ENTER)
		{
			this.addSite();
		}
	},

	showDomainHint: function()
	{
		var box = this._domainInput;
		// Make sure the box isn't focused if we're going to 
		// show the hint...
		if (!box.get('focused'))
		{
			if (!dojo.trim(box.get('value')))
			{
				box.set('value', this.DOMAIN_HINT);
				dojo.addClass(box.domNode, 'textBoxHint');
			}
		}
	},

	hideDomainHint: function()
	{
		var box = this._domainInput;
		if (box.get('value') == this.DOMAIN_HINT)
		{
			dojo.removeClass(box.domNode, 'textBoxHint');
			box.set('value', '');
		}
	},

	copyDomainsFromCommonList: function()
	{
		/* Summary:
		 * 	Brings nodes from the common list tree
		 * 	over to the blocked sites tree.
		 */
		var that = this;
		var itemsToAdd = this.commonListTree.getSelectedItems(true);

		dojo.forEach(itemsToAdd, function(item)
		{
			var parent = that.blockListTree.getSelectedGroup(true, true);
            var name = that.commonListStore.getValue(item, 'name');
            var type = that.commonListStore.getValue(item, 'type');
            var domain = that.commonListStore.getValue(item, 'domain');

            that.blockListTree.addItemToTree(parent, name, type, domain);
		});
	},

	addSite: function()
	{
		/* Summary:
		 * 	Takes the domain text box input and adds an
		 * 	item to the block list tree corresponding to the
		 * 	domain. Adds to the group that is selected in the
		 * 	tree.
		 */
		var siteName = this._domainInput.get('value');

		if (siteName)
		{
			var parent = this.blockListTree.getSelectedGroup(true, true);
            // Prepend the *. to the start of the domain
            var domain = '*.' + siteName;
            var type = 'individual';

			// Add the domain, the group should be set by now
			if (parent)
			{
				this.blockListTree.addItemToTree(parent, siteName, type, domain);
			}
		}

		// Clear the input
		this._domainInput.reset();

		// We need this here because the onBlur calls while
		// the textbox still has value, and skips putting
		// the tooltip... so we do it manually
		this.showDomainHint(); 
	},

	createNewGroupFolder: function()
	{
		/* Summary:
		 * 	When the user drags an item to the new group
		 * 	folder, create another new group folder so they
		 * 	have the ability to create even more groups. There
		 * 	should only be one existing new group folder
		 * 	at a time, and it should have an attribute 'newGroup'
		 * 	set to true.
		 */
		var that = this;

		this.blockListStore.fetchItemByIdentity({
			identity: that.lastID,
			onItem: function(item) 
			{
				if (item !== null)
				{
					// No longer a "newGroup" type folder
					that.blockListStore.setValue(item, 'newGroup', false);
				}
			}
		});

		this.lastID = this.blockListStore.getIdentity(this.blockListStore.newItem({
			name: this.NEW_GROUP_FOLDER_NAME,
			type: "group",
			newGroup: true,
			children: []
		}));
		
		// New group added, need to update the group names
		this.setGroupNames();

		// The selected group is now likely to be viable for seing its settings
		// but the click event wont fire off, so we need to manually call this
		// method
		this.treeSelectionChanged();
	},

	setGroupNames: function()
	{
		// Summary:
		// 	Whenever groups change (ex. a group added, group(s) deleted
		// 	group moved) Make sure the group name numbers stay in order
		// 	in the blockListTree

		var that = this;
		var curGroupNum = 0;

		this.blockListStore.fetch({
			query: { 'type': 'group' },
			onItem: function(item) {
				if (!that.blockListStore.getValue(item, 'newGroup'))
				{
					// Set the name of the folder to 
					// "Group #" which represents the next new group
					// increment the group number field
					that.blockListStore.setValue(item, 'name', 'Group ' + (++curGroupNum));
				}
			}
		});
			
	},
	
	dndCheckDrop: function(target, source, position)
	{
		var that = this;
		var targetTree = dijit.getEnclosingWidget(target).tree;

		if (targetTree == this.blockListTree)
		{
			var targetItem = dijit.getEnclosingWidget(target).item;
			var store = targetTree.model.store;

			if (store.getValue(targetItem, 'type') == 'group')
			{
				return true;
			}
		}
		return false;
	},

	dndCheckDrag: function(source, nodes) 
	{
		var that = this;
		var value = false;

		dojo.forEach(nodes, function(node)
		{
			var item = dijit.getEnclosingWidget(node).item;
			var store = source.tree.model.store;

			if (store.getValue(item, 'type') == 'individual')
			{
				value = true;
			}
		});

		return value;
	},

	itemAddedToBlockList: function(item, parentInfo)
	{
		if (parentInfo && parentInfo.item)
		{
			if (this.blockListStore.getIdentity(parentInfo.item) == this.lastID)
			{
				// Create a new folder
				this.createNewGroupFolder();
			}
		}
	}
});
})();

