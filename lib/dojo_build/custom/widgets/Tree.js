(function(){
dojo.provide('shockcollar.widgets.Tree');

dojo.require('dijit.Tree');
dojo.require('dojo.data.ItemFileWriteStore');
dojo.require('dojox.html.styles');

dojo.declare('shockcollar.widgets.Tree', [dijit.Tree], {

	CSS_SHEET_NAME: 'SetupAndSettings.css',

    constructor: function()
    {
        this.inherited(arguments);

        var that = this;

        this.showRoot = false;
		this.autoExpand = true;
		this.openOnClick = false;
		this.openOnDblClick = true;
		this.dragThreshold = 5;
    }, 

	addItemToTree: function(/*ItemFileReadStoreItem*/ group, /*ItemFileReadStoreItem*/ name,
        type, domain)
	{
		/* Summary:
		 * 	Used for adding an individual domain to a
		 * 	group list of domains. The group is the
		 * 	parent node of the domain in the tree.
         * Note:
         *  TODO: Check for duplicates
		 */
        var that = this;
        var alreadyExists = false;

        // Also Check if duplicate
        this.model.getChildren(group,
            /*onComplete*/ function(items)
            {
                // Only if there aren't existing items
                dojo.forEach(items, function(item)
                {
                    if (that.model.store.getValue(item, 'domain') == domain)
                    {
                        alreadyExists = true;
                    }
                });

                if (!alreadyExists)
                {
                    // Add the new item (setting the proper parentInfo)
                    that.model.store.newItem({
                        name: name,
                        type: type,
                        domain: domain
                    },
                    {
                        parent: group,
                        attribute: 'children'
                    });
                }
            },
            /*onError*/ function()
            {
                // Stick 'er in anyways!
                // Add the new item (setting the proper parentInfo)
                that.model.store.newItem({
                    name: name,
                    type: type,
                    domain: domain
                },
                {
                    parent: group,
                    attribute: 'children'
                });
            });
	},

    makeStoreData: function(callback)
    {
        /* Summary:
         *  Take the items in this tree and recreate the store data in such a way
         *  that it is re-useable by other trees and can be used by the add-on code
         *  to look to see block list data.
         */
        if (!callback)
        {
            var deferred = new dojo.Deferred();
        }
        var model = this.model;
		var store = model.store;
		var that = this;
		var groupsLeftToIterate = 0;

        // The object that is eventually returned when the deferred is over...
		var dataStore = {
			'label': 'name',
			'items': [] 
		};

        store.fetch(
        {
            query: { 'type': 'group' },
            onComplete: function(groups)
            {
                // Do not include the 'NewGroup'
                groupsLeftToIterate = groups.length;

                // Special case no block groups
                if (groupsLeftToIterate == 0) 
                {
                    if (!callback)
                    {
                        deferred.callback(dataStore);
                    }
                    else
                    {
                        callback(dataStore);
                    }
                }

                dojo.forEach(groups, function(group)
                {
                    if (!store.getValue(group, 'newGroup')){
                        var groupName = store.getValue(group, 'name');

                        model.getChildren(group, function(children)
                        {
                            var groupChildren = [];

                            dojo.forEach(children, function(child)
                            {
                                var childName = store.getValue(child, 'name');
                                var childDomain = store.getValue(child, 'domain');
                                var childType = store.getValue(child, 'type');

                                groupChildren.push({name: childName, type: childType, domain: childDomain });
                            });

                            dataStore.items.push({ name: groupName, type: 'group', children: groupChildren });	
                            groupsLeftToIterate--;

                            if (groupsLeftToIterate <= 0)
                            {
                                if (!callback)
                                {
                                    deferred.callback(dataStore);
                                }
                                else
                                {
                                    callback(dataStore);
                                }
                            }
                        });
                    }
                    else
                    {
                        // is a new group
                        groupsLeftToIterate--;

                        if (groupsLeftToIterate <= 0)
                        {
                            if (!callback)
                            {
                                deferred.callback(dataStore);
                            }
                            else
                            {
                                callback(dataStore);
                            }
                        }
                    }
                });
            }
        });

        if (!callback)
        {
            return deferred;
        }
    },

	getSelectedGroup: function(/*bool*/ defaultToFirst, /*bool*/ lookAtInnerSelection)
	{
		/* Summary:
		 * 	Gets the selected group of the specified tree. If no group
		 * 	default to the first group (if the bool defaultToFirst is true)
		 * defaultToFirst:
		 * 	Set to true if a group should always be returned.
		 * 	Default to first means default to the first group.
		 * lookAtInnerSelection:
		 * 	Set to true if you want the group to be returned of a
		 * 	selected leaf node. Set to false if the group needs
		 * 	to be selected, and it cannot be an inner node of the
		 * 	group.
		 */
		var group;
		var store = this.model.store;
		
		if (this.get('selectedItems'))
		{
			dojo.some(this.get('selectedItems'), function(item) 
			{
				if (store.getValue(item, 'type') == 'group')
				{
					group = item;

					// Break from the loop
					return false;
				}
			});

			// Looks like the selected item did not include a group...
			// Lets look at the selected items and take the parent group:
			if (lookAtInnerSelection && typeof group == 'undefined')
			{
				// Gets the path to the selected item in the tree
				var pathItems = this.get("path");
				
				// Let us pray that this returns a group!
				dojo.some(pathItems, function(pathItem) {
					// In case we get invalid item arguments (in the case of the root esp.)
					try
					{
						if (store.getValue(pathItem, 'type') == 'group')
						{
							group = pathItem;
							
							// Break from the for loop
							return false;
						}
					}
					catch (e)
					{

					}
				});
			}
		}
		

		if (defaultToFirst && typeof(group) == 'undefined')
		{
			store.fetch({
				query: { 'type': 'group' },
				onItem: function(item) {
					if (typeof(group) == 'undefined')
					{
						group = item;
					}
				}
			});
		}

		return group;
	},

	getSelectedItems: function(/*bool*/ includeGroups)
	{
		/* Summary:
		 * 	Returns all the individuals selected in
		 * 	a given tree.
		 * includeGroups (bool):
		 * 	If true, include individuals and groups
		 */
		var selectedItems = [];
		var store = this.model.store;

		dojo.forEach(this.get('selectedItems'), function(item) {
			if (store.getValue(item, 'type') == 'individual')
			{
				selectedItems.push(item);
			}
			else if (includeGroups && store.getValue(item, 'type') == 'group')
			{
				selectedItems.push(item);	
			}

		});

		return selectedItems;
	},

	// Used in BlockLists and BlockSchedule for getting
	// the proper icon for a tree node
	getIconClass: function(/*dojo.data.Item*/ item, /*Bool*/ opened)
	{
		/* Summary:
		 * 	Override the default getIconClass in dijit.Tree.
		 * 	Look for a favorite icon using the domain property
		 * 	of the node. Give the "new folder" node a unique 
		 * 	icon.
		 */
        var store = this.model.store;

		try
		{
			if (store.getValue(item, 'type') == 'group' )
			{
				// If this is truthy, than it must be a newGroup folder
				if (store.getValue(item, 'newGroup'))
				{
					return 'newGroupIcon';
				}
				return (opened ? 'dijitFolderOpened': 'dijitFolderClosed');
			}
			else 
			{
				// Try to find the websites favicon using google's favicon service
				// Make sure to get rid of invalid chars in domain
				var cssUnique = store.getValue(item, 'name').split('.').join('');
				var cssClassName = 'dijitLeaf' + cssUnique;
				try
				{
					var favIconURL = "http://www.google.com/s2/favicons?domain="
							+ store.getValue(item, 'name');
					var declaration = "height: 16px; width: 16px; background-image: url(\"" 
							+ favIconURL + "\");";

					// Check if an icon actually exists (was returned)
					// NOTE: this proved unnecessary since google will return a default
					// globe icon in the case that the domain is not found
					/*var deferred = dojo.xhrGet({
						url: favIconURL
					});*/

					dojox.html.insertCssRule('.' + cssClassName, declaration, this.CSS_SHEET_NAME);
				}
				catch (e)
				{
					return 'dijitLeaf';
				}

				return cssClassName;
			}
		}
		catch (e)
		{
			return 'dijitLeaf';	
		}
	}

});
})();

