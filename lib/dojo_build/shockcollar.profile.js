dependencies = {
    layers: [
    {
        "name": "shockcollar.js",
        "dependencies":
        [
            "shockcollar.panels.BlockLists",
            "shockcollar.panels.BlockSchedule",
            "shockcollar.panels.Deterrent",
            "shockcollar.panels.Initiation",
            "shockcollar.panels.Panels",
            "shockcollar.panels.TermsAndConditions",
            "shockcollar.panels.Welcome",
            "shockcollar.widgets.Tree",
            "shockcollar.util.Util",
            "dojo.NodeList-traverse"
        ]
    }],
    prefixes: [
        ["shockcollar", "../custom"],
        ["dijit", "../dijit"],
        ["dojox", "../dojox"]
    ]
};
