/* 
 * settings.js
 *
 *  This module is in charge of the initial setup for the user which is
 * opened immediately after install, and shouldn't be needed every again.
 * 
 */
const SS = require('simple-storage');
const Data = require('self').data;
const Tabs = require('tabs');
const PageMod = require('page-mod').PageMod;

const Governor = require('block-governor');
const Menu = require('menu');
const Util = require('util');

// Constants
const FILE_SETUP_AND_SETTINGS_CS = Data.url('setup/SetupAndSettingsCS.js');
const FILE_JQUERY = Data.url('jquery-1.7.1.min.js');

exports.init = init;
exports.openSettings = openSettings;
exports.openSetup = openSetup;
exports.FILE_SETTINGS_HTML = Data.url('setup/Settings.html');
exports.FILE_SETUP_HTML = Data.url('setup/Setup.html');

var _settingsPageMod;
var _setupPageMod;

function init()
{
    /* Summary:
     *  Initiate the settings page mod so that when it is opened we attach
     *  the approriate content scripts. Block the settings from the user 
     *  when any block is active. Do the same for setup.
     */
    if (_settingsPageMod)
    {
        _settingsPageMod.destroy();
    }

    _settingsPageMod = PageMod({
        include: [exports.FILE_SETTINGS_HTML + '*'],
        contentScriptWhen: 'start',
        contentScriptFile: [FILE_JQUERY,
                            FILE_SETUP_AND_SETTINGS_CS],
        onAttach: function(worker) {
            // If anything is blocked, block the settings page. Cheat time cannot
            // be used to open settings.
            if (!Governor.getBlockReleased() && Governor.getBlockState())
            {
                Governor.blockPage(worker);
            }
            else
            {
                // If the user payed the punishment for a block release, then we
                // should display in the settings that once the settings are saved
                // then blocks will be re-initiated.
                if (Governor.getBlockReleased()) 
                {
                    worker.port.emit('BlockReleased');
                } 
                
                // Used for saving the settings the user specified during setup
                worker.port.on('SavePanelData', function(payload) {
                    payload[2] = true; // Set to true because we are saving from settings
                    exports.savePanelData(payload)
                });

                worker.port.on('RetrievePanelData', function(source)
                {
                    /* source (string):
                     *  The name of the panel to retrieved saved panel data for.
                     */
                    console.log('Retrieve Panel Data source: ' + source);
                    var savedPanelData = SS.storage[source];
                    worker.port.emit('LoadPanelData', [source, savedPanelData]);
                });

                worker.port.on('Close', function() 
                {
                    console.log('Worker tab close');
                    worker.tab.close();
                });
            }
        }
    });

    if (_setupPageMod)
    {
        _setupPageMod.destroy();
    }

    // SETUP PAGE MOD
    _setupPageMod = PageMod({
        include: [exports.FILE_SETUP_HTML + '*'],
        contentScriptWhen: 'start',
        contentScriptFile: [FILE_JQUERY,
                            FILE_SETUP_AND_SETTINGS_CS],
        onAttach: function(worker) {
            if (!Governor.getBlockReleased() && Governor.getBlockState())
            {
                Governor.blockPage(worker);
            }
            else
            {
                // Don't block things while we are in setup
                // this level of block release makes sure facebook is still 
                // accessible while settings are being saved and a deterrent
                // of facebook is selected
                Governor.setBlockReleased(true);

                var currentStep = SS.storage.currentStep;
                if (currentStep)
                {
                    worker.port.emit('LoadStep', currentStep);
                }

                // Used for saving the settings the user specified during setup
                worker.port.on('SavePanelData', function(payload) {
                    payload[2] = false;     // false because we're not saving from settings
                    exports.savePanelData(payload);
                });

                worker.port.on('RetrievePanelData', function(source)
                {
                    /* source (string):
                     *  The name of the panel to retrieved saved panel data for.
                     */
                    console.log('Retrieve Panel Data source: ' + source);
                    var savedPanelData = SS.storage[source];
                    worker.port.emit('LoadPanelData', [source, savedPanelData]);
                    // Save what step we are on
                    // Note that for blockschedule, this wont work properly since we will
                    // load the block lists right after the block schedule and that will
                    // count for the last loaded step.
                    SS.storage.currentStep = source;
                });

                worker.port.on('FinishedSetup', function()
                {
                    SS.storage.finished_setup = true;
                    console.log('Finished setup, unreleasing block');
                    Governor.setBlockReleased(false);
                    worker.tab.close();
                });
            }
        }
    });
}

function openSetup()
{
    // Only open the setup if it's not open, otherwise switch to its tab
    // The setup is opened only if the user has explicitly opened it from the menu
    // or if the add-on was just installed. If the setup was not finished
    // for any reason the widget menu will only have a link to open the 
    // setup.
    
    // Only open if not already open!
    for each(var tab in Tabs)
    {
        if (tab.url == exports.FILE_SETUP_HTML)
        {
            // is open...
            tab.activate();
            return;
        }
    }

    // Open the setup and attach a worker script that looks for when the 
    // settings are saved.
    Tabs.open({
        url: exports.FILE_SETUP_HTML
    });
}

function openSettings()
{
    // First check if the tab is already open, if it is then activate it
    for each (var tab in Tabs)
    {
        if (tab.url == exports.FILE_SETTINGS_HTML)
        {
            // is open...
            tab.activate();
            return;
        }
    }

    // Open it up in a fresh tab ONLY IF NOT ALREADY OPEN
    Tabs.open({
        url: exports.FILE_SETTINGS_HTML
    });
}

exports.savePanelData = function(payload)
{
    /* source (string):
     *  The name of the panel that we're saving data for.
     * data (JSON string of object):
     *  The data to be saved for the particular source panel
     */
    var source = payload[0];
    var data = payload[1];
    var settings = payload[2]; // true if saving from settings, false if from setup

	console.log('in save panel: ' + source + ' data: ' + data);
    SS.storage[source] = data;

    // Optional per-panel special during save instructions
    if (source == 'BlockLists')
    {
        Governor.updateBlockLists(JSON.parse(data));
    }
    else if (source == 'BlockSchedule')
    {
        Governor.updateSchedule(JSON.parse(data));
    }
    else if (source == 'Deterrent')
    {
        // Don't forget that the data is stringified
        var dataObj = JSON.parse(data);

        // Unrelease the block if it was released after a deterrent has been reactivated
        console.log('Payload settings: ' + settings);
        if (settings && Governor.getBlockReleased())
        {
            Governor.setBlockReleased(false);
        }

        // Special cases after save
        if (dataObj.selectedDeterrent == 'embarrassment')
        {

        }
        else if (dataObj.selectedDeterrent == 'currency')
        {

        }
        else if (dataObj.selectedDeterrent == 'password')
        {

        }
    }
};

