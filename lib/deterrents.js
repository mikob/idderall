/*
 * Key
 *  This module is used when the user selects intervention as their deterrent.
 *  The user chooses an email to to send a key to. We send a key that will allow
 *  the users instance to unlock. 
 *
 * Facebook
 *  This module takes care of the embarrasment deterrent choice. Facebook
 *  users are first authenticated, and then if the user decides to suffer
 *  punishment this module will take care of updating their status message.
 *
 * PayPal
 *  This module is used when the user selects the payment punishment. When
 *  the user selects an amount, and enables the punishment, we store the amount
 *  in add-on storage and on the shockcollar server along with a unique instance
 *  key. When (if) the user decides to suffer punishment, the amount payed must
 *  match the amount we have stored on the servers previously. If the amount 
 *  and payment check out, we can ask the server is the user has paid, and then
 *  release the governor temporarily allowing the user to modify settings.
 */
const SS = require('simple-storage');
const Tabs = require('tabs');
const PageMod = require('page-mod').PageMod;
const Request = require('request').Request;

const Governor = require('block-governor');

// Vars
const SHOCKCOLLAR_DOMAIN = 'http://idderall.com';

// var Password = 
// {
//     SHOCKCOLLAR_ADD_PASS_INST_URL: SHOCKCOLLAR_DOMAIN + '/key/register',
//     SHOCKCOLLAR_CHECK_PASS_URL: SHOCKCOLLAR_DOMAIN + '/key/validate',
// 
//     // function init()
//     // {
//     //     /* Summary:
//     //      *  Make a new instance of the password. We need to make a new instance
//     //      *  anytime a new key is generated. This will happen in case the user
//     //      *  changes the recepient of the password, or if the user give's up
//     //      *  and we need to send a new password to the recepient.
//     //      *
//     //      * Description:
//     //      *  The new password needs to be sent out to the recepient via email.
//     //      *  We let the idderall servers handle this.
//     //      *  The server returns the instance (which acts as user name) after it
//     //      *  generates a password for it and sends out the corresponding email.
//     //      */
// 
//     //     Request({
//     //         url: SHOCKCOLLAR_ADD_PASS_INST_URL,
//     //         content: {
//     //             'key': key
//     //         },
//     //         onComplete: function(response) {
//     //             // When this is finished we can release the 
//     //             // governor
//     //             console.log('shockcollar fb servers post ' + response.status);
//     //             if (response.text == 'Success')
//     //             {
//     //                 Governor.releaseBlock('password');
//     //             }
//     //         },
//     //         onError: function(err){
//     //             console.log(err);
//     //         }
//     //     }).post();
// 
//     // }
// 
//     authenticate: function(instanceKey, password)
//     {
//         /* Summary:
//          *  Called anytime the user decides to enter the password and release 
//          *  the block. If the password is authentic, then we can release the
//          *  block.
//          *
//          * instanceKey [hash str]:
//          *  The instance key the server auto-generated and returned on success
//          *  when the email form was sent.
//          * 
//          * password [str]:
//          *  The password the user entered.
//          *
//          * Description:
//          *  Attempt to authenticate the password against the idderall database
//          *  for it's corresponding instance.
//          */
// 
//         function serverError(msg)
//         {
// 
//         }
// 
//         Request({
//             url: SHOCKCOLLAR_KEY_AUTH_URL,
//             content: {
//                 'instance_key': instanceKey,
//                 'password': password
//             },
//             onComplete: function(response) {
//                 // When this is finished we can release the governor
//                 if (response.text == 'Success')
//                 {
//                     Governor.releaseBlock('password');
//                 }
//                 else 
//                 {
//                     serverError(msg);
//                 }
//             },
//             onError: function(err){
//                 serverError(msg);
//             }
//         }).get();
//     }
// 
// };

var Facebook = 
{
    // FB_GRAPH_POST_STATUS: '/feed/',   //eg https://graph.facebook.com/{id}/feed/
    // FB_GRAPH_URL: 'https://graph.facebook.com/',
    // FB_AUTH_DOMAIN: '*.facebook.com',
    // FB_AUTH_DONE_URL: 'https://www.facebook.com/connect/login_success.html',
    // FB_AUTH_URL:  "https://www.facebook.com/dialog/oauth?client_id=135451879893783&\
    //             redirect_uri=https://www.facebook.com/connect/login_success.html\
    //             &scope=publish_stream&response_type=token",
    // SHOCKCOLLAR_FB_URL: SHOCKCOLLAR_DOMAIN + '/fb_user/register',
    SHOCKCOLLAR_FB_PUNISH_URL: SHOCKCOLLAR_DOMAIN + '/facebook/punish',

    // _FBAuthPageMod: null,
    // _setupPanel: null,

    // authenticateUser: function(setupPanel)
    // {
    //     /* Summary:
    //      *  Open a tab with facebook OAuth and wait for it to redirect
    //      *  to success. If we have success, store the users credentials
    //      *  locally in the add on and allow them to proceed with setup.
    //      */
    //     var that = this;
    //     this._setupPanel = setupPanel;

    //     this._FBAuthPageMod = PageMod({
    //         include: this.FB_AUTH_DOMAIN,
    //         contentScriptWhen: 'start',
    //         onAttach: function(worker) {
    //             // We need to user regular expressions here to check if the success
    //             // url is open, and then to extract the access_token from that
    //             // success url
    //             var url = worker.tab.url;
    //             console.log('Page worker attached to: ' + url);

    //             try
    //             {
    //                 var authDoneUrlPattern = new RegExp(this.FB_AUTH_DONE_URL, 
    //                     'ig')
    //                     .exec(url);

    //                 authDoneUrlPattern = authDoneUrlPattern[0];

    //                 var accessToken = new RegExp('(?:' + this.FB_AUTH_DONE_URL
    //                     + ')#access_token=(.*)', 'i')
    //                     .exec(url);

    //                 accessToken = accessToken[1].split('&')[0];

    //                 console.log(authDoneUrlPattern);
    //                 console.log(accessToken);

    //                 if (accessToken != null && authDoneUrlPattern != null)
    //                 {
    //                     that.checkFBCredentials(accessToken, worker.tab);
    //                 }
    //             }
    //             catch (err)
    //             {
    //                 console.log(err);
    //             }
    //         }
    //     });

    //     Tabs.open({
    //         url: this.FB_AUTH_URL
    //     });

    // },

    // checkFBCredentials: function(accessToken, tab)
    // {
    //     /* Summary:
    //      *  Using the access token we received from the redirect, check the
    //      *  facebook graph api to make sure that we indeed have a valid
    //      *  access token. If the access token is valid, we can store it locally
    //      *  along with other info we extracted from the users graph. We also
    //      *  need to send it to be saved on the server? perhaps?
    //      */
    //     var that = this;

    //     Request({
    //         url: this.FB_GRAPH_URL + 'me',
    //         content: {
    //             'access_token': accessToken
    //         },
    //         onComplete: function(response)
    //         {
    //             console.log(response.text);
    //             var graphObj = JSON.parse(response.text);
    //             console.log(graphObj);

    //             try
    //             {
    //                 var id = graphObj.id;
    //                 var gender = graphObj.gender;

    //                 that._storeFBCredentials(id, gender, accessToken);

    //                 tab.close();
    //                 that._FBAuthSuccess();
    //             }
    //             catch (err)
    //             {
    //                 tab.close();
    //                 that._FBAuthFail();
    //             }
    //         }
    //     }).get();
    // },

    // _storeFBCredentials: function(id, gender, accessToken)
    // {
    //     SS.storage.FBCreds = { 
    //         'id': id,
    //         'gender': gender,
    //         'accessToken': accessToken
    //     };
    // },

    // _FBAuthSuccess: function()
    // {
    //     this._setupPanel.port.emit('FBAuthSuccess', null);
    //     this._FBAuthPageMod.destroy();
    // },

    // _FBAuthFail: function()
    // {
    // },

    punish: function()
    {
        /* Summary:
         *  This is the method that needs to be called when the
         *  user decides to suffer the facebook punishment. It
         *  will post an embarrassing status message at a random time
         *  between maximum and minimum values determined server side.
         *  The facebook status message is also determined server-side.
         */
        var detData = JSON.parse(SS.storage.Deterrent);
        var fbId = detData.fbId;
        var instanceKey = detData.instanceKey;
        console.log('FB Punish fbId: ' + fbId + ' instanceKey: ' + instanceKey);

        // Post to shockcollar db
        Request({
            url: this.SHOCKCOLLAR_FB_PUNISH_URL,
            content: {
                'instance_key': instanceKey,
                'fb_id': fbId
            },
            onComplete: function(response) {
                // When this is finished we can release the 
                // governor
                // TODO: remove next line
                console.log('shockcollar fb servers post ' + response.status);
                Governor.releaseBlock('embarrassment');
            },
            onError: function(err){
                console.log(err);
            }
        }).post();
    }

    /* UNUSED
    function postStatus()
    {
        /* Summary:
         *  Post to FB using this client side code
         *
        console.log('FB Post Status hit');
        var comment = 'test graph api 3';
        var accessToken = SS.storage.FBCreds.accessToken;
        var fbId = SS.storage.FBCreds.id;

        Request({
            url: FB_GRAPH_URL + fbId + FB_GRAPH_POST_STATUS,
            content: {
                    'message': comment,
                    'access_token': accessToken
                },
            onComplete: function(response)
            {
                console.log(response.text);
                var graphObj = JSON.parse(response.text);

                try
                {
                    if (graphObj.id !== null)
                    {
                        // Post was successful
                    }
                }
                catch (err)
                {
                    // There was an error posting
                }
            }
        }).post();
    };*/
};

var PayPal = {
    SHOCKCOLLAR_CHECK_PAYPAL_USER_URL: SHOCKCOLLAR_DOMAIN + '/paypal/status',

    checkIfPayed: function()
    {
        /* Summary:
         *  Check if the user has paid due punishment
         */
        var selectedDeterrent;
        var instanceKey;
        var waitingForPaypalPayment = SS.storage.waitingForPaypalPayment;

        try 
        {
            var detData = JSON.parse(SS.storage.Deterrent);
            selectedDeterrent = detData.selectedDeterrent;
            instanceKey = detData.instanceKey;
        }
        catch (e) { }

        // Check if the user has succesfully made payment to unblock deterrent here
        console.log('Check if PAYED PAL: ' + selectedDeterrent + ' waiting: ' + waitingForPaypalPayment);
        if (selectedDeterrent == 'currency' && waitingForPaypalPayment == true)
        {
            console.log('Hitting Servers for PAYPAL response: ' + instanceKey);
            // Post to shockcollar db
            Request({
                url: this.SHOCKCOLLAR_CHECK_PAYPAL_USER_URL, 
                content: {
                    'instance_key': instanceKey,
                },
                onComplete: function(response) {
                    // When this is finished we can release the governor
                    console.log('shockcollar paypal servers: ' + response.status);
                    if (response.text == 'Success')
                    {
                        Governor.releaseBlock('currency');
                    }
                },
                onError: function(err){
                    console.log('shockcollar paypal server error: ' + err);
                }
            }).get();
        }
    }

};

// exports.Password = Password;
exports.Facebook = Facebook;
exports.PayPal = PayPal;

