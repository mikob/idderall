exports.assert = assert;
exports.error = error;
exports.warn = warn;

function AssertException(message) { this.message = message; }

AssertException.prototype.toString = function () {
  return 'AssertException: ' + this.message;
}

function assert(exp, message) {
  if (!exp) {
    throw new AssertException(message);
  }
}

function error(msg)
{
    console.log('ERROR: ' + msg);
}

function warn(msg)
{
    console.log('WARN: ' + msg);
}
