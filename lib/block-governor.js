/*
 * block-governor.js
 *
 *  This module is in charge of governing all the page blocking that is necessary.
 * It also takes care of cheat minutes, and releasing blocks if the user has
 * suffered the punishment they have chosen.
 */
const Data = require('self').data;
const PageMod = require('page-mod').PageMod;
const {MatchPattern} = require('match-pattern');
const SS = require('simple-storage');
const Tabs = require('tabs');

//TODO: REMOVE THE DEBUG ASSET
const assert = require('debug').assert;
const Util = require('util');
const Settings = require('settings');
const Deterrents = require('deterrents');
const Sheriff = require('sheriff');
const Notify = require('notify');
const TimeWidget = require('time-widget');       //used for access to cheat time widget
const Scheduler = require('scheduler');
const Facebook = Deterrents.Facebook;
const PayPal = Deterrents.PayPal;

var _blockWorkers = [];
var _blockedPageWorkers = [];
var _passwordSurrenderPageWorkers = [];
var _currencySurrenderPageWorkers = [];
var _blockLists;
var _schedule;
var _mainPageMod;   // The page mod for sites on the block lists
var _blockPageMod;  // The page mod for blocked page workers
var _passwordSurrenderPageMod;
var _currencySurrenderPageMod;
var _blockActiveWhenLastChecked;
var _activeGroupEvents = {};        // The scheduled events for an active block
var _openGroups = [];

const MILLISECS_IN_DAY = (24 * 60 * 60 * 1000);
const WEEKDAYS = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday',
      'friday', 'saturday'];
// Notice that the cheat replenish time can be easily updated to a variable in
// the future, so the advanced user can decide how often to replenish cheat time.
const CHEAT_REPLENISH_TIME  = (24 * 60 * 60 * 1000); // 24 hrs
const FILE_BLOCK_PAGE_HTML  = Data.url('frames/BlockPage.html');
const FILE_BLOCK_PAGE_CS    = Data.url('frames/BlockPageCS.js');
const FILE_PAYPAL_HTML      = Util.remoteFile(true, 'frames/PaypalSurrender.html');
const FILE_PAYPAL_CS        = Data.url('frames/PaypalSurrenderCS.js');
const FILE_PASSWORD_HTML    = Util.remoteFile(true, 'frames/PasswordSurrender.html');
const FILE_PASSWORD_CS      = Data.url('frames/PasswordSurrenderCS.js');
const FILE_JQUERY           = Data.url('jquery-1.7.1.min.js');

exports.updateBlockLists = updateBlockLists;
exports.init = init;
exports.updateSchedule = updateSchedule;
exports.releaseBlock = releaseBlock;
exports.getBlockState = getBlockState;
exports.blockPage = blockPage;
exports.getBlockReleased = getBlockReleased;
exports.setBlockReleased = setBlockReleased;
exports.domainFromURL = domainFromURL;
exports.getRemCheatTime = getRemCheatTime;
exports.getBlockGroupByURL = getBlockGroupByURL;
exports.isGroupCheating = isGroupCheating;
exports.FILE_BLOCK_PAGE_HTML = FILE_BLOCK_PAGE_HTML;


// This needs to be cleared on browser restart
// If it is 1, the 2-5 warning has been delivered, if it is 2 then the 3-60s
// warning has been delivered.
var _notificationsSent = {}; 

function init()
{
    if (typeof SS.storage.waitingForPaypalPayment == 'undefined' ||
        SS.storage.waitingForPaypalPayment == null)
    {
        SS.storage.waitingForPaypalPayment = false;
    }

    /* 
     * The block page mod is attached to the block page html files.
     */
    _blockPageMod = PageMod({
        include: [FILE_BLOCK_PAGE_HTML + '*'],
        contentScriptWhen: 'start',
        contentScriptFile: [FILE_JQUERY,
                            FILE_BLOCK_PAGE_CS],
        onAttach: function(worker) {
            _blockedPageWorkers.push(worker);

            worker.on('detach', function() {
                console.log('Blocked page worker DETACHED');
                _detachPageWorker(this, _blockedPageWorkers);
            });

            worker.on('Destroy', function() {
                _detachPageWorker(this, _blockedPageWorkers);
            });

            // Pass off convenience information useful to the user
            worker.port.emit('Load', _getBlockPageInfo(worker));

            worker.port.on('GiveUp', giveUp);
        }
    });

    function getInstanceKey()
    {
        try
        {
            var data = JSON.parse(SS.storage.Deterrent);
            console.log('get instance key from init');
            return data.instanceKey;
        }
        catch(err)
        {
        }
    }

    function getCurrencyAmount()
    {
        try
        {
            var data = JSON.parse(SS.storage.Deterrent);
            return data.currencyAmount;
        }
        catch (err)
        {
        }
    }

    // Password surrender page mod
    _passwordSurrenderPageMod = PageMod({
        include: [FILE_PASSWORD_HTML + '*'],
        contentScriptWhen: 'end',
        contentScriptFile: [FILE_JQUERY,
                            FILE_PASSWORD_CS],
        onAttach: function(worker) {
            _passwordSurrenderPageWorkers.push(worker);

            worker.on('detach', function() {
                console.log('Password surrender worker DETACHED');
                _detachPageWorker(this, _passwordSurrenderPageWorkers);
            });

            worker.on('Destroy', function() {
                _detachPageWorker(this, _passwordSurrenderPageWorkers);
            });

            worker.port.emit('Load', getInstanceKey());

            worker.port.on('Unblock', function() {
                releaseBlock('password');
                worker.tab.close();
            });
        }
    });

    _currencySurrenderPageMod = PageMod({
        include: [FILE_PAYPAL_HTML + '*'],
        contentScriptWhen: 'end',
        contentScriptFile: [FILE_JQUERY,
                            FILE_PAYPAL_CS],
        onAttach: function(worker) {
            _currencySurrenderPageWorkers.push(worker);

            worker.on('detach', function() {
                console.log('Paypal surrender worker DETACHED');
                _detachPageWorker(this, _currencySurrenderPageWorkers);
            });

            worker.on('Destroy', function() {
                _detachPageWorker(this, _currencySurrenderPageWorkers);
            });

            worker.port.emit('Load', { 
                'currencyAmount': getCurrencyAmount(),
                'instanceKey': getInstanceKey()
            });

            worker.port.on('Unblock', function() {
                releaseBlock('currency');
                worker.tab.close();
            });
        }
    });

}

/* Summary:
 *  Get the block release state. 
 *
 * NOTE:
 *  For paypal, we must poll the server to see if the paypal payment
 *  has been made. 
 *
 * return [Boolean]:
 *  True if the block has been released (the punishment can be 
 *  assumed to be payed after this point), false otherwise.
 */
function getBlockReleased()
{
    console.log('Get block released: ' + SS.storage.blockReleased);
    return SS.storage.blockReleased;
}

function setBlockReleased(val)
{
    console.log('Set block released: ' + val);
    SS.storage.blockReleased = Boolean(val);

    // The cheat timer needs to be active/unactive depending
    // on the inverse of the block release state.
    TimeWidget.setActive(!val); 
}

function getBlockState()
{
    /* Summary:
     *  Performs actions when isAnyBlockActive has changed since it's last 
     *  call. Useful for notifying components that block state has changed.
     *
     * Description:
     *  Improves the performance of checking the addons. Does
     *  heavy checking if the block is released (checks paypal).
     *  Since disabling uninstall takes a few seconds, the user won't have 
     *  to deal with the delay if some other action invoked checking the 
     *  block state and we noticed it was changed here. 
     *
     * WARNING:
     *   This is not called instantaneously when the block state is changed,
     *   rather, when the user performs an action that needs to check
     *   the block state.
     *
     * returns BOOL:
     *    Whether any block is active.
     */

    // The check if payed will release the block if it needs to
    PayPal.checkIfPayed();

    var blockActive = _isAnyBlockActive() && !getBlockReleased() ;

    // if block state changed...
    if (blockActive != _blockActiveWhenLastChecked) {
        // The methods that would be interested in knowing
        // that the block state has changed
        Sheriff.allowUninstall(!blockActive);

        _blockActiveWhenLastChecked = blockActive;
    }

    return blockActive;
}

function isGroupBlocked(groupName)
{
    /* Summary:
     *  Checks if the given groupName is actively
     *  blocked.
     *
     * returns [bool]:
     *  whether blocked or not.
     */

    return _getBlockBlob(groupName).currentlyBlocked;
}

/* Summary:
 *  Check if a group is currently cheating (should also be deducting
 *  the cheat timer in this case.
 */
function isGroupCheating(groupName)
{
    if (!getBlockReleased())
    {
        scheduleInfo = _getBlockBlob(groupName);
        if (scheduleInfo.currentlyBlocked === true
            && SS.storage.CheatData
            && SS.storage.CheatData[groupName]
            && SS.storage.CheatData[groupName].remCheatTime > 0)
        {
            return true;
        }
    }

    return false;
}

function updateBlockLists(blockLists)
{
    var that = this;
    _blockLists = blockLists;

    // Get all the domains we need to attach the page mod to.
    var blockedDomains = [];

    for (group in blockLists.items)
    {
        var group = blockLists.items[group];

        for (child in group.children)
        {
            var child = group.children[child];
            var blockedDomain = child.domain;

            blockedDomains.push(blockedDomain);
        }
    }
            
    // This might need to be cleaned up, since the included block
    // domains are updated, unlike other page mods
    if (_mainPageMod)
    {
        _mainPageMod.destroy();
    }

    /* 
     * Attach a worker to each blocked page
     */
    _mainPageMod = PageMod({
        include: blockedDomains,
        contentScriptWhen: 'start',
        onAttach: function(worker) {
            if (worker.tab && _isWorkerUnique(worker))
            {
                var groupName = getBlockGroupByURL(worker.tab.url);
                worker.groupName = groupName;
                
                // If there is a group name, we know the domain exists on a block 
                // list
                if (groupName)
                {
                    _blockWorkers.push(worker);

                    worker.on('detach', function() {
                        console.log('Potential blocked page worker detached.');
                        _detachWorker(this);
                    });

                    _checkBlocked(groupName);
                    TimeWidget.activate(worker.tab);

                }
            }
        }
    });
}

/* Summary:
 *   A worker is attached to urls that are sometimes not even opened. We
 *   use this method to check if the worker is genuinely its own tab.
 */
function _isWorkerUnique(worker)
{
    for (var i = 0; i < _blockWorkers.length; i++)
    {
        var existingWorker = _blockWorkers[i];
        //console.log('existing workers tab indeces = ' + existingWorker.tab.index);
        if (existingWorker.tab.index === worker.tab.index)
        {
            // Note there is a worker.url which is set to a url for each onAttach
            // event, and a worker.tab.url which is the actual location displayed
            // in the tab
            assert(existingWorker.tab.url == worker.tab.url, '_isWorkerUnique() ' + 
                    'Urls should be the same for identical tab index.');
            return false;
        }
    }

    return true;
}

function _detachWorker(worker)
{
    var groupName = worker.groupName;
    var index = _blockWorkers.indexOf(worker);
    if (index != -1)
    {
        _blockWorkers.splice(index, 1);
    }

    // Check if it's the last open in its group, if it is
    // remove it from the openGroups, and remove the scheduled
    // items.
    var lastOpenInGroup = true;
    for (var i = 0; i < _blockWorkers.length; i++)
    {
        if (_blockWorkers[i].groupName === groupName)
        {
            lastOpenInGroup = false;
            break;
        }
    }
    
    console.log('Last open in group: ' + lastOpenInGroup);

    if (lastOpenInGroup)
    {
        if (_activeGroupEvents[groupName])
        {
            // This will cancel any pending events
            for (var k = 0; k < _activeGroupEvents[groupName].length; k++)
            {
                Scheduler.removeEvent(_activeGroupEvents[groupName][k]);
            }

            // Proper way to clear an array
            _activeGroupEvents[groupName].length = 0;
        }

        var openGroupIndex = _openGroups.indexOf(groupName);
        console.log('_openGroupIndex: ' + openGroupIndex);
        _openGroups.splice(openGroupIndex, 1);
    }

    worker.destroy();
}

function _detachPageWorker(worker, arr)
{
    var index = arr.indexOf(worker);
    if (index != -1)
    {
        arr.splice(index, 1);
    }
    worker.destroy();
}

/* Summary:
 *  This is the most important function for checking whether a page is blocked. 
 *  First we will check that the worker being checked in fact does have a tab,
 *  and that the block isn't in a released state.
 */
function _checkBlocked(groupName)
{
    if (!getBlockReleased())
    {
        scheduleInfo = _getBlockBlob(groupName);
        scheduleInfo.cheatTime = getRemCheatTime(groupName);

        console.log(groupName + ' scheduleInfo.timeUntilBlock: ' + scheduleInfo.timeUntilBlock
                + ' currentlyBlocked: ' + scheduleInfo.currentlyBlocked);

        // If there is no time left until this is blocked
        if (scheduleInfo.currentlyBlocked == true && 
            scheduleInfo.cheatTime <= 0)
        {
            // Block the whole group, the page should get blocked along
            // with the group
            blockGroup(groupName);
        }
        // Page is not exactly blocked at the moment
        else 
        {
            // This can be null if something is "never" blocked
            if (scheduleInfo.timeUntilBlock !== null)
            {
                if (_openGroups.indexOf(groupName) === -1)
                {
                    _openGroups.push(groupName);
                    console.log('Adding ' + groupName + ' to _openGroups');

                    if (scheduleInfo.currentlyBlocked == true &&
                        scheduleInfo.cheatTime > 0)
                    // Page should be blocked atm, but there is remaining cheat
                    // time
                    {
                        // Keep track of when we started cheat time so we know when to
                        // replenish
                        SS.storage.CheatData[groupName].startedCheatTime = new Date();
                        // When the block should be checked again...
                        var changeInStateTime = new Date(new Date().getTime() + 
                                scheduleInfo.cheatTime * 1000);

                        console.log('Currently blocked and in cheat time. Cheat time end: '
                            + changeInStateTime);

                        var checkBlockEventID = Scheduler.addEventWithUpdate(
                                changeInStateTime, 
                                (function() {
                                    _openGroups.splice(_openGroups.indexOf(groupName), 1);
                                    _checkBlocked(groupName);
                                }).bind(this),
                                (function() {
                                    SS.storage.CheatData[groupName].remCheatTime = 
                                        Math.floor((changeInStateTime - new Date()) / 1000);
                                }).bind(this)
                        );

                        // Notification warnings
                        _handleNotifications(groupName, 'cheat', changeInStateTime);

                        if (!_activeGroupEvents[groupName])
                            _activeGroupEvents[groupName] = [];

                        _activeGroupEvents[groupName].push(checkBlockEventID);

                        // Call this so that if the currently selected tab should display cheat
                        // time remaining, it does. Otherwise we wait until the tabs are activated
                        // to activate the time widget
                        TimeWidget.activate(Tabs.activeTab);
                    }
                    // Will be blocked soon
                    else
                    {
                        var changeInStateTime = new Date(new Date().getTime()
                                + scheduleInfo.timeUntilBlock);

                        console.log('Will be blocked soon: ' + changeInStateTime);

                        var checkBlockEventID = Scheduler.addEvent(
                                changeInStateTime, 
                                (function() {
                                    // Remove from open groups, 
                                    // there should no longer be anything scheduled
                                    _openGroups.splice(_openGroups.indexOf(groupName), 1);
                                    _checkBlocked(groupName);
                                }).bind(this)
                        );

                        // Notification warnings
                        _handleNotifications(groupName, 'block', changeInStateTime);

                        if (!_activeGroupEvents[groupName])
                            _activeGroupEvents[groupName] = [];

                        _activeGroupEvents[groupName].push(checkBlockEventID);
                    }

                }
                console.log('open groups already contains group');
            }
            console.log('never blocked according to the schedule');
        }
    }
    else
    {
        console.log("Block is in released state");
    }
}

/* Summary:
 *  Take care of notification pop-ups for the user. Send a notification
 *  when an event is about to occur in advance. For now we handle when
 *  a group is about to be blocked, or cheat time is running out.
 *
 * groupName [String]:
 *  Name of the group to warn for.
 *
 * type [String]:
 *  Either 'cheat' or 'block'. 
 *      'cheat'  -- Warn the user that cheat time is running out for the group
 *      'block'  -- Warn the user that an active group is about to be blocked
 *
 * time [Datetime]:
 *  The time when the event will actually occur.
 * 
 */
function _handleNotifications(groupName, type, time)
{
    var now = new Date();
    var FIVE_MINUTES = 5 * 60 * 1000;
    var THIRTY_SECONDS = 30 * 1000;

    if (!_activeGroupEvents[groupName])
        _activeGroupEvents[groupName] = [];

    var msg;
    if (type === 'cheat')
    {
        // {0} group name
        // {1} number
        // {2} units (minutes, seconds)
        msg = "{1} cheat {2} remain for {0}";
    }
    else 
    {
        msg = "{0} will be blocked in {1} {2}";
    }

    console.log(' Notification for ' + groupName + ' at ' + (time - FIVE_MINUTES));

    // More than 5 minutes away
    if (time - now > FIVE_MINUTES)
    {
        var fiveWarnTime = 5;
        var fiveWarnUnits = 'minutes';

        // Schedule a notification for 5 minutes before the time
        var fiveMinNoticeEventID = Scheduler.addEvent(
                time - FIVE_MINUTES, 
                (function() {
                    Notify.msg(msg.format(groupName, fiveWarnTime, fiveWarnUnits)); 
                }).bind(this)
        );
        _activeGroupEvents[groupName].push(fiveMinNoticeEventID);
    }

    // More than 30 seconds away
    if (time - now > THIRTY_SECONDS)
    {
        var warnTime = 30;
        var warnUnits = 'seconds';

        // Schedule a notification for 5 minutes before the time
        var thirtySecNoticeEventID = Scheduler.addEvent(
                time - THIRTY_SECONDS, 
                (function() {
                    Notify.msg(msg.format(groupName, warnTime, warnUnits)); 
                }).bind(this)
        );
        _activeGroupEvents[groupName].push(thirtySecNoticeEventID);
    }

}

function updateSchedule(schedule)
{
    _schedule = schedule;

    // Clear the remaining cheat time and any other volatile temporary variables
    SS.storage.CheatData = null; // this forces reset of cheat time

    /* Communicate to the block workers that the schedule has changed
    console.log('exports.blockworker.length: ' + _blockWorkers.length);
    if (_blockWorkers.length > 0)
    {
        console.log('emitting schedule change');

        for (var i = 0; i < _blockWorkers.length; i++)
        {
            _blockWorkers[i].port.emit('ScheduleChanged', blockSchedule);
        }
    }*/
}

function releaseBlock(deterrentType)
{
    /* Summary:
     *  This is called if the user has payed the punishment due. Get all 
     *  the block page workers and automatically load the pages that they 
     *  should be on.
     *
     * Description:
     *  Makes a notification that the block was released succesfully. Called
     *  after giveUp();
     *
     * deterrentType [string]:
     *  Either currency, embarrassment or ...
     */
    var msg = '';

    if (deterrentType == 'currency')
    {
        msg = 'Your payment has been received. ';
        SS.storage.waitingForPaypalPayment = false;
    }
    else if (deterrentType == 'embarrassment')
    {
    }
    else if (deterrentType == 'password')
    {
        msg = 'Key unlock successful. '
    }

    setBlockReleased(true);

    // Notify the blocked page workers
    console.log('blockworkers length ' + _blockedPageWorkers.length);
    for (var i = 0; i < _blockedPageWorkers.length; i++) 
    {
        _blockedPageWorkers[i].port.emit('BlockRelease');
    }

    // Remove all scheduled events, since they have to do with 
    // blocks in active state
    for (var j = 0; j < _blockLists.items.length; j++)
    {
        var groupName = _blockLists.items[j].name;

        if (_activeGroupEvents[groupName])
        {
            for (var k = 0; k < _activeGroupEvents[groupName].length; k++)
            {
                Scheduler.removeEvent(_activeGroupEvents[groupName][k]);
            }

            // Proper way to clear an array
            _activeGroupEvents[groupName].length = 0;
        }
    }

    Notify.msg(msg + 'All blocks lifted indefinitely until you update'
                  + ' the deterrent settings.');
}

function giveUp()
{
    // Check the chosen deterrent and initiate
    // appropriate action based on it
    var deterrent = JSON.parse(SS.storage.Deterrent).selectedDeterrent;
    console.log('giving up ' + deterrent);

    if (deterrent == 'currency')
    {
        // This page will handle the donations and releasing the block
        // open in a fresh tab, so that the users pages are correctly returned
        // to what the user wanted to access originally.
        Tabs.open({
            url: FILE_PAYPAL_HTML
        });

        SS.storage.waitingForPaypalPayment = true;
    }
    else if (deterrent == 'embarrassment')
    {
        // The block will be released internally
        // in this punish function
        Facebook.punish();
    }
    else if (deterrent == 'password')
    {
        Tabs.open({
            url: FILE_PASSWORD_HTML
        });
    }
}

function _isAnyBlockActive()
{
    /* Summary:
     *  Check if there is any group actively blocked
     * WARNING:
     *  Usually getBlockState is called unless there is a good reason
     *  for this to be called.
     */
    if (typeof _blockLists != 'undefined')
    {
        for (group in _blockLists.items)
        {
            var group = _blockLists.items[group];
            if (isGroupBlocked(group.name))
            {
                return group.name;
            }
        }
    }
    return false;
}

function getBlockGroupByURL(url)
{
    /* Summary:
     *  Returns the name of the group that the domain is a part of.
     *  Uses a regular expression to see if a domain matches.
     *
     * Considerations:
     *  If the domain is part of two groups... this will only return the first group.
     */
    var groupName;

    for (group in _blockLists.items)
    {
        var group = _blockLists.items[group];

        for (child in group.children)
        {
            var child = group.children[child];
            var blockedDomainPattern = new MatchPattern(child.domain);
            
            if (blockedDomainPattern.test(url))
            {
                return group.name;
            }
        }
    }

    return groupName;
}

function domainFromURL(url)
{
    /* Summary:
     *  Return the domain of a given URL.
     */
    var httpSplit = url;

    try
    {
        httpSplit = url.split('://')[1];
        httpSplit = httpSplit.split('/')[0];
    }
    catch (err)
    {
    }

    return httpSplit;
}

function _getBlockPageInfo(worker)
{
    /* Summary:
     *  Gather pertinent block information for the user's convenience so we
     *  can display it to them on the block page.
     *
     * Returns:
     * {
     *  msg [str]: Says which group is blocked and until what time.
     *  cheatMsg [str]: Tells user when cheat time will be replenished (null if
     *      the group does not have cheat mode activated)
     * }
     *
     */
    console.log('_getBlockPageInfo: ' + worker.tab.url);

    var ret = null;
    var groupName = null;
    var origUrl = null;

    // Get the original url from the hash info
    try
    {
        origUrl = decodeURIComponent(unescape(worker.tab.url.split('#url=')[1]));
        if (origUrl == Settings.FILE_SETTINGS_HTML)
        {
            groupName = 'settings';
        }
        else if (origUrl == Settings.FILE_SETUP_HTML)
        {
            groupName = 'setup';
        }
        else
        {
            groupName = getBlockGroupByURL(origUrl);
        }

    }
    catch(e)
    {
        console.log(e);
    }
    console.log('origUrl: ' + origUrl + ' groupName: ' + groupName);

    if (groupName) {
        if (groupName == 'settings')
        {
            return {
                msg: "Settings are locked while a block is active."
            };
        } 
        if (groupName == 'setup')
        {
            return {
                msg: "Setup is locked while a block is active."
            };
        }
        var now = new Date();
        var formattedTime = '-';
        var msg = '';
        var cheatMsg = '';
        var formattedCheatTime = '-';
        var timeUntilFree = _getBlockBlob(groupName).timeUntilFree;
        var freeDate = new Date(new Date().setTime(now.getTime() + timeUntilFree));

        if (freeDate.getDay() == now.getDay()
            && freeDate - now < MILLISECS_IN_DAY)
        {
            // Will happen sometime today!
            formattedTime = Util.formatDateTime('time', freeDate);
        }
        else
        {
            formattedTime = Util.formatDateTime('datetime', freeDate);
        }
        msg = groupName + ' is blocked until ' + formattedTime;

        // Cheat replenish time
        // Redundant, but safe!
        getRemCheatTime(groupName); // Do this to make sure cheat time is safe
        if (_schedule[groupName].cheatMode === true && 
                SS.storage.CheatData[groupName].startedCheatTime)
        {
            console.log(SS.storage.CheatData[groupName].startedCheatTime);
            var cheatReplenishDate = new Date(new Date().setTime(
                    new Date(SS.storage.CheatData[groupName].startedCheatTime).getTime() 
                + CHEAT_REPLENISH_TIME));
            if (cheatReplenishDate.getDay() == now.getDay()
                    && cheatReplenishDate - now < MILLISECS_IN_DAY)
            {
                // Will happen sometime today!
                formattedCheatTime = Util.formatDateTime('time', cheatReplenishDate);
            }
            else
            {
                formattedCheatTime = Util.formatDateTime('datetime', cheatReplenishDate);
            }
            cheatMsg = 'Cheat time will be replenished for ' + groupName + 
                ' at ' + formattedCheatTime;
        }

        ret = {
            msg: msg,
            cheatMsg: cheatMsg
        };
    }

    return ret;
}

function blockGroup(groupName)
{
    console.log('Blocking group ' + groupName);
    for (var i = 0; i < _blockWorkers.length; i++)
    {
        if (_blockWorkers[i].groupName == groupName)
        {
            blockPage(_blockWorkers[i]);
        }
    }
}

function blockPage(worker)
{
    /* Summary:
     * 	Block the page we are currently on. The _blockPageMod should take care
     * 	of gathering pertinent block group information and handing it to the
     * 	BlockPage.
     */

    worker.tab.attach({
        contentScript: 'window.stop();'
    });

    // Add the original url in the hash so the user can refresh the original
    // page that was blocked. 
    var origUrl = escape(encodeURIComponent(worker.tab.url));
    var newUrl = FILE_BLOCK_PAGE_HTML + '#url=' + origUrl;

    worker.tab.url = newUrl;
}

function _getBlockEnd(scheduleEntry, inTempBlock)
{
    /* Summary:
     *  Blobs overlapping block schedules together so the actual end date of
     *  an active block can be determined.
     *
     * inTempBlock [bool]:
     *  Make true if we're in a temporary block (the current block is a temp one)
     *  Make false if we're in a scheduled block
     *
     * Assumptions:
     *  We are currently in an active block.
     *
     * Returns:
     *  The time until a block is over in milliseconds.
     */
    var now = new Date();
    var dayOfWeek = WEEKDAYS[now.getDay()];
    var timeUntilBlockEnd = 0;
    var timeUntilTempBlockEnd = 0;

    // This should definitely resolve to true if we're calling this
    // and there is no temp block
    if (scheduleEntry.activeDays[dayOfWeek])
    {
        if (scheduleEntry.activeTime.allDay)
        {
            var numDays = 0;
            for (var i = now.getDay(); i < now.getDay() + WEEKDAYS.length; i++)
            {
                // i starts at the current weekday and makes its way around
                var dayNum = i % WEEKDAYS.length;
                var dayOfWeek = WEEKDAYS[dayNum];
                if (scheduleEntry.activeDays[dayOfWeek])
                {
                    numDays++;
                }
                else 
                {
                    // we hit an inactive day
                    break;
                }
            }

            var blockDate = new Date(now.getTime() + numDays * MILLISECS_IN_DAY);
            blockDate.setHours(0);
            blockDate.setMinutes(0);
            blockDate.setSeconds(0);
            blockDate.setMilliseconds(0);

            timeUntilBlockEnd = blockDate - now;
        }
        else 
        {
            timeUntilBlockEnd = Util.strToDate(scheduleEntry.activeTime.timeEnd) - now;
        }
    }

    if (inTempBlock)
    {
        timeUntilTempBlockEnd = new Date(scheduleEntry.tempBlock.timeEnd) - now;
        if (timeUntilTempBlockEnd > timeUntilBlockEnd)
        {
            timeUntilBlockEnd = timeUntilTempBlockEnd;
        }
    }

    return timeUntilBlockEnd;
}

function getRemCheatTime(groupName)
{
    /* Summary:
     *  Gets the number of seconds remaining in a given groups cheat time.
     *
     * Other info:
     *  Also replenishes the cheat time if a threshold time has passed
     *
     * Returns [int]
     *  The number of seconds remaining in groupName's cheat quota.
     */

    var cheatTable = SS.storage.CheatData;
    var scheduleEntry = _schedule[groupName];
    var now = new Date();
    var calculatedTime = 0;

    if (cheatTable && cheatTable[groupName] 
            && cheatTable[groupName].startedCheatTime)
    {
        console.log('cheat replenish? ' + (now - new Date(cheatTable[groupName].startedCheatTime)));
        if (cheatTable[groupName].startedCheatTime && 
            now - new Date(cheatTable[groupName].startedCheatTime) > CHEAT_REPLENISH_TIME)
        {
            calculatedTime = scheduleEntry.cheatMins * 60;

            // we replenished the cheat time... so restart the counter
            SS.storage.CheatData[groupName].startedCheatTime = null;
        }
        else
        {
            // No adjustments to the stored time should be made in this condition
            console.log('time remaining: ' + cheatTable[groupName].remCheatTime);
            return cheatTable[groupName].remCheatTime;
        }
    }
    else
    {
        if (!cheatTable) {
            SS.storage.CheatData = {};
        }

        // Create the initial cheat data for this group
        if (scheduleEntry.cheatMode === true)
        {
            calculatedTime = scheduleEntry.cheatMins * 60;
        }
    }

    SS.storage.CheatData[groupName] = {
        remCheatTime: calculatedTime
    };
    console.log('time remaining: ' + SS.storage.CheatData[groupName].remCheatTime);
    return calculatedTime;
}

function _getBlockBlob(groupName)
{
    /* Summary:
     *  Getting the block start date is simply a matter of checking if the 
     *  schedule is actively blocked all day, if not checking if it is blocked
     *  for the current time, otherwise looking at the temporary times and seeing
     *  if the current time falls under that time frame. If nothing is currently
     *  blocked, then return whichever block comes first (whether it is the temp
     *  block, or the schedule block. The timeUntilBlockFree is more complex,
     *  see _getBlockEnd()
     *
     * Other info:
     *  timeUntilFree returns 0 if there is not a block currently active
     *
     * groupName [str]:
     *  There is a schedule entry for every group name.
     *
     * Return:
     *       An object with these values...
     *      {
     *        currentlyBlocked [bool],  // true if blocked atm
     *        timeUntilBlock [int],     // milliseconds until the schedule would
     *                                  // be considered blocked (active)
     *        timeUntilFree [int]       // milliseconds until the schedule will
     *                                  // be considered unblocked (dormant). If
     *                                  // already free, return 0. WARNING: returns
     *                                  // when the current block is free (there
     *                                  // could be another block immediately 
     *                                  // following)
     *      }
     */
    var now = new Date();
    var dayOfWeek = WEEKDAYS[now.getDay()];
    var scheduleEntry = _schedule[groupName];
    
    // Defaults
    var currentlyBlocked = false, timeUntilBlock = null, timeUntilFree = 0;
    
    if (typeof scheduleEntry != 'undefined')
    {
        // Check if on an scheduled active day
        if (scheduleEntry.activeDays[dayOfWeek])
        {
            // Check if conforms to scheduled day time period
            if (scheduleEntry.activeTime.allDay ||
                now >= Util.strToDate(scheduleEntry.activeTime.timeStart)
                && now <= Util.strToDate(scheduleEntry.activeTime.timeEnd))
            {
                // This can be returned immediately since 
                // an immediate block has the highest priority
                return {
                    currentlyBlocked: true,
                    timeUntilBlock: 0,
                    timeUntilFree: _getBlockEnd(scheduleEntry, false)
                };
            }
            else if (now <= Util.strToDate(scheduleEntry.activeTime.timeStart))
            // If the block is not all day, but will happen later in the day
            {
                // At this point we're not in a scheduled block
                // The time until the next chronologically nearest scheduled block
                var scheduleUntilBlock = Util.strToDate(scheduleEntry.activeTime.timeStart) -
                    now;

                // timeUntilFree is 0, because we are currently free 
                timeUntilBlock = scheduleUntilBlock;
            }
            else 
            // The block is not all day, and is already over 
            // (now > Util.strToDate(scheduleEntry.activeTime.timeEnd
            {
                var day;
                var i = 0;
                do
                {
                    i++;
                    day = WEEKDAYS[(now.getDay() + i) % 7];
                    if (scheduleEntry.activeDays[day])
                    {
                        var scheduleUntilBlock = new Date((i * MILLISECS_IN_DAY) 
                                + Util.strToDate(scheduleEntry.activeTime.timeStart).getTime())
                                - now;

                        // timeUntilFree is 0, because we are currently free 
                        timeUntilBlock = scheduleUntilBlock;
                    }
                }
                while (timeUntilBlock == null && day != dayOfWeek);
            }
        }
        else
        // if it's not on an active day
        {
            var day;
            var i = 0;
            do
            {
                i++;
                day = WEEKDAYS[(now.getDay() + i) % 7];
                if (scheduleEntry.activeDays[day])
                {
                    var scheduleUntilBlock = null;
                    if (scheduleEntry.activeTime.allDay)
                    {
                        var then = new Date();
                        then.setHours(0);
                        then.setMinutes(0);
                        then.setSeconds(0);
                        then.setMilliseconds(0);
                        scheduleUntilBlock = new Date((i * MILLISECS_IN_DAY) 
                                + then.getTime())
                                - now;
                    }
                    else
                    {
                        scheduleUntilBlock = new Date((i * MILLISECS_IN_DAY) 
                                + Util.strToDate(scheduleEntry.activeTime.timeStart).getTime())
                                - now;

                    }
                    // timeUntilFree is 0, because we are currently free 
                    timeUntilBlock = scheduleUntilBlock;
                }
            }
            while (timeUntilBlock == null && day != dayOfWeek);
        }
        // The temporary block 
        // Can a temporary block start at an arbitrary time? yes
        // check which block is occuring first, thats what is returned
        if (scheduleEntry.tempBlock)
        {
            // temporary block objects are stored as js date strings
            if (now >= new Date(scheduleEntry.tempBlock.timeStart)
               && now <= new Date(scheduleEntry.tempBlock.timeEnd))
            {
                return {
                    currentlyBlocked: true,
                    timeUntilBlock: 0,
                    timeUntilFree: _getBlockEnd(scheduleEntry, true)
                };
            }
            else
            {
                // the time until the next closest (chronologically) temporary block
                tempUntilBlock = new Date(scheduleEntry.tempBlock.timeStart) -
                    now;
                if (timeUntilBlock == null || tempUntilBlock < timeUntilBlock)
                {
                    timeUntilBlock = tempUntilBlock;
                }
            }
        }
    }

    return {
        currentlyBlocked: currentlyBlocked,
        timeUntilBlock: timeUntilBlock,
        timeUntilFree: timeUntilFree
    }
}

// Ancillary -- taken from SO
String.prototype.format = function() {
  var args = arguments;
  return this.replace(/{(\d+)}/g, function(match, number) { 
    return typeof args[number] != 'undefined'
      ? args[number]
      : match
    ;
  });
};
