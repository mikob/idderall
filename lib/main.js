// Import the kit modules we need.
const SS = require('simple-storage');
const Windows = require('windows').browserWindows;
const timers = require("timers");
// const Data = require('self').data;

const Menu = require('menu.js');
const TimeWidget = require('time-widget');
const Settings = require('settings.js');
const Governor = require('block-governor.js');
const Sheriff = require('sheriff');

exports.main = function(options, callbacks) 
{
    if (options.loadReason == 'install')
    {
        SS.storage.finished_setup = false;
        Settings.openSetup();
    }
    else if (options.loadReason == 'startup'
        && SS.storage.finished_setup !== true)
    {

    }

    // exports.reset(); 
    Sheriff.init();      // Disable uninstall and enforce other rules
    Governor.init();
    Settings.init();
    Menu.init();        // The menu icon widget
    TimeWidget.init();

    // Initiate the blockers
    if (SS.storage.BlockLists)
    {
        var blockLists = JSON.parse(SS.storage.BlockLists);
        Governor.updateBlockLists(blockLists);
    }

    if (SS.storage.BlockSchedule)
    {
        var schedule = JSON.parse(SS.storage.BlockSchedule);
        Governor.updateSchedule(schedule);
    }
};

exports.reset = function()
{
    // Panels:
    SS.storage.BlockLists = null;
    SS.storage.BlockSchedule = null;
    SS.storage.Deterrent = null;
    SS.storage.TermsAndConditions = null;

    // ancillary data
    SS.storage.finished_setup = null;
    SS.storage.blockReleased = null;
    SS.storage.CheatData = null;
    SS.storage.currentStep = null; // keeping track of the current step in setup
    SS.storage.timeWidgetActive = null;

    console.log('New quota: ' + SS.quotaUsage * 100 
        + '%');
};

// If the user uninstalls or disables the add-on
exports.onUnload = function(reason) 
{
	console.log('unloading ' + reason);
    if (reason == 'shutdown')
    {
    } 
    else if (reason == 'uninstall' || reason == 'disable')
	{

	}
};
