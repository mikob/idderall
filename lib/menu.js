/* 
 * menu.js
 *
 *  This module is in charge of the menu attached to the widget in the 
 *  firefox add-on bar. The menu is dynamic depending on factors such as
 *  whether the user has finished setup, what buttons are pressed on the
 *  menu, etc.
 */
const Main = require('main');
const Panel = require('panel').Panel;
const Widget = require('widget').Widget;
const SS = require('simple-storage');
const Data = require('self').data;
const Governor = require('block-governor');
const Settings = require('settings');
const Tabs = require('tabs');
const Util = require('util');

// Other consts
const PANEL_WIDTH = 253;
const QUICK_BLOCK_PANEL_WIDTH = 273;
const MAIN_PANEL_HEIGHT = 97;
const SETUP_PANEL_HEIGHT = 40;
const ADD_SITE_PANEL_HEIGHT = 315;
const QUICK_BLOCK_PANEL_HEIGHT = 245;
const WIDGET_ICON_URL = Data.url('media/logox32.png');
const FILE_MENU_HTML = Data.url('menu/Menu.html');
const FILE_MENU_CS = Data.url('menu/MenuCS.js');
const FILE_JQUERY = Data.url('jquery-1.7.1.min.js');

var _menuPanel = null;

exports.init = function()
{
    _menuPanel = Panel({
        width: PANEL_WIDTH,
        height: MAIN_PANEL_HEIGHT,
        allow: { script: true },
        contentURL: FILE_MENU_HTML,
        contentScriptFile: [ FILE_JQUERY, FILE_MENU_CS]
    });

    Widget({
        id: 'shockcollar-menu',
        label: 'Idderall Menu',
        contentURL: WIDGET_ICON_URL,
        panel: _menuPanel
    });

    _menuPanel.on('show', function() { 
        if (SS.storage.finished_setup)
        {
            _menuPanel.resize(PANEL_WIDTH, MAIN_PANEL_HEIGHT);
            _menuPanel.port.emit('ShowMain');
            loadSettings();
        }
        else
        {
            _menuPanel.resize(PANEL_WIDTH, SETUP_PANEL_HEIGHT);
            _menuPanel.port.emit('ShowContinueSetup');
        }
    });


    // This works similarly to setup...
    function loadSettings()
    {
        // Just receive the panel data for all the panels that are needed
        var domain = Governor.domainFromURL(Tabs.activeTab.url);
        var blockList = SS.storage.BlockLists;
        var blockSchedule = SS.storage.BlockSchedule;
        _menuPanel.port.emit('LoadSettings', [blockList, blockSchedule, domain]);
    }

    _menuPanel.on('hide', function()
    {
        _menuPanel.port.emit('Hide');
        _menuPanel.resize(PANEL_WIDTH, MAIN_PANEL_HEIGHT);
    });

    _menuPanel.port.on('Hide', function()
    {
        // manual hiding
        _menuPanel.hide();
    });

    _menuPanel.port.on('SaveSettings', Settings.savePanelData);

    _menuPanel.port.on('OpenSetup', function()
    {
        console.log('Calling open setup from menu.js');
        Settings.openSetup();
        _menuPanel.hide();
    });

    _menuPanel.port.on('ChangeSettings', function()
    {
        console.log('Calling open settings from menu.js');
        Settings.openSettings();    
        _menuPanel.hide();
    });

    _menuPanel.port.on('LoadAddToBlocklist', function()
    {
        console.log('Calling add site to block group from menu.js');
        _menuPanel.resize(PANEL_WIDTH, ADD_SITE_PANEL_HEIGHT);
    });

    _menuPanel.port.on('LoadQuickBlock', function()
    {
        console.log('Calling quick block from menu.js');
        _menuPanel.resize(QUICK_BLOCK_PANEL_WIDTH, QUICK_BLOCK_PANEL_HEIGHT);
    });
};
