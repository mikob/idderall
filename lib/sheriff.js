/* The sheriff is in charge of making sure the user doesn't
 * break the rules. Currently:
 *      -Prevents user from disabling the addon during a block
 *
 * TODO:
 *      -Prevent proxy use
 *      -Prevent user from downloading other browsers
 */
const self = require('self');
const Governor = require('block-governor');
const Observers = require('observer-service');
const {Cc, Ci, Cr} = require('chrome');

Components.utils.import("resource://gre/modules/Services.jsm");
Components.utils.import("resource://gre/modules/FileUtils.jsm");
Components.utils.import("resource://gre/modules/AddonManager.jsm");

exports.init = init;
exports.allowUninstall = allowUninstall;

const KEY_PROFILEDIR = "ProfD";
const ORIG_EXT_SQL_FILE = "extensions.sqlite";

// Default to allowing the uninstall, once a block is active
// and we disable the uninstall, this will be set to false.
var allowingUninstall = true;

function init()
{
    Observers.add("chrome-document-global-created", function(wnd) {
        if (wnd.location.href == "about:addons") {
            console.log("addons access detected");

            Governor.getBlockState();

        }
    });

    Observers.add("xpi-provider-shutdown", function(wnd) {
		console.log("xpi provider shutdown!");

		let allow = allowingUninstall;

        let origDBFile = FileUtils.getFile(KEY_PROFILEDIR, [ORIG_EXT_SQL_FILE], true);
        let dbConn = Services.storage.openUnsharedDatabase(origDBFile); // Will also create the file if it does not exist

		let SQLstmt = "UPDATE addon SET type='' WHERE id=" + "'" + self.id + "'";
		if (allow)
		{
			SQLstmt = "UPDATE addon SET type='extension' WHERE id=" + "'" + self.id + "'";
		}

        dbConn.executeSimpleSQL(SQLstmt);
        dbConn.close();
		delete dbConn;
        
		// Start the add-on manager and allow it to make an exclusive connection
		// to the db
        AddonManagerPrivate.startup();
    });
}

function allowUninstall(/* bool */ allow)
{

    /* Summary: 
     *   Does its best to prevent the user from disabling the addon via
     *   the addons manager.
     */

	console.log('allow uninstall');

    // Check if there is a change in allowUninstall state
    if (allow != allowingUninstall) {

		// Shutdown the addon manager temporarily so we can make a connection
		// to the db
		AddonManagerPrivate.shutdown();
		allowingUninstall = allow;
    }
}
