/* 
 * time-widget.js
 *
 * This module runs the cheat timer that counts down the
 * number of seconds the user has remaining in cheat time.
 */
const Widget = require('widget').Widget;
const Tabs = require('tabs');
const Timer = require('timers');
const SS = require('simple-storage');

const Util = require('util');
const Governor = require('block-governor');

const PIXEL_WIDTH_PER_CHAR = 7;

exports.init = init;
exports.activate = activate;
exports.setActive = setActive;

var cheatTimeWidgetUpdateTimerID = null;
var _active = null;

function init()
{
    Tabs.on('activate', function(tab)
    {
        activate(tab);
    });

    if (typeof(SS.storage.timeWidgetActive) === 'undefined'
        || SS.storage.timeWidgetActive === null)
    {
        SS.storage.timeWidgetActive = true;
    }
    _active = SS.storage.timeWidgetActive;
}

exports.cheatTimeWidget = Widget({
    id: 'cheat-time-widget',
    label: 'Cheat Time Remaining',
    content: ' ',
    width: 0
});

exports.cheatTimeWidget.updateCheatTime = function(cheatSeconds)
{
    var msg = String(Math.floor(cheatSeconds / 60)) + ':' + 
            Util.padZeroes(cheatSeconds % 60, 2);
    this.width = PIXEL_WIDTH_PER_CHAR * msg.length;
    this.content = "<span style='margin-top: 6px; font-size: 12px; font-family: Arial;'>" 
            + msg + "</span>";
};

exports.cheatTimeWidget.clear = function()
{
    this.content = ' ';
    this.width = 1;
};

/* Summary:
 * The active boolean allows us to control a switch on the timer. Whether
 * it is on or off. If it is off, the interval timer will not be running.
 * 
 * val [Boolean]:
 *  Set to true to allow timer to run and cheat minutes to be displayed.
 *  Set to false to hide the timer (for example if block is released)
 */
function setActive(val)
{
    SS.storage.timeWidgetActive = val;
    _active = val;
}


/* Summary:
 *  Call this when a new tab is opened that is considered a 'cheating' tab.
 *  It will display the timer, if there is cheat minutes for the group.
 */
function activate(tab)
{
    // Update the remaining cheat time widget everytime a tab changes
    var groupName = Governor.getBlockGroupByURL(tab.url);
    if (cheatTimeWidgetUpdateTimerID)
    {
        Timer.clearInterval(cheatTimeWidgetUpdateTimerID);
    }

    if (groupName && _active && Governor.isGroupCheating(groupName))
    {
        var remCheatTime = Governor.getRemCheatTime(groupName);
        if (remCheatTime > 0)
        {
            exports.cheatTimeWidget.updateCheatTime(remCheatTime);
            cheatTimeWidgetUpdateTimerID = Timer.setInterval((function() {
                if (tab == Tabs.activeTab && _active)
                {
                    var remCheatTime = Governor.getRemCheatTime(groupName);
                    if (remCheatTime <= 0)
                    {
                        exports.cheatTimeWidget.clear();
                        Timer.clearInterval(cheatTimeWidgetUpdateTimerID);
                    }
                    exports.cheatTimeWidget.updateCheatTime(remCheatTime);
                }
                else
                {
                    exports.cheatTimeWidget.clear();
                    Timer.clearInterval(cheatTimeWidgetUpdateTimerID);
                }
            }).bind(this), 1000);
        }
        else
        {
            exports.cheatTimeWidget.clear();
        }
    }
    else
    {
        exports.cheatTimeWidget.clear();
    }

}
